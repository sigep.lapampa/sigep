<?php
/* Smarty version 3.1.39, created on 2023-02-23 16:14:42
  from 'C:\AppServ\www\vtigercrm75\layouts\v7\modules\Settings\PickListDependency\ListViewRecordActions.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_63f790f2a1b497_06983596',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '1eb4202266349ac1f649b6229ed11e5ce4af7fd2' => 
    array (
      0 => 'C:\\AppServ\\www\\vtigercrm75\\layouts\\v7\\modules\\Settings\\PickListDependency\\ListViewRecordActions.tpl',
      1 => 1669872319,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_63f790f2a1b497_06983596 (Smarty_Internal_Template $_smarty_tpl) {
?><div class="table-actions"><?php $_smarty_tpl->_assignInScope('RECORD_SOURCE_MODULE', $_smarty_tpl->tpl_vars['LISTVIEW_ENTRY']->value->get('sourceModule'));
$_smarty_tpl->_assignInScope('RECORD_SOURCE_FIELD', $_smarty_tpl->tpl_vars['LISTVIEW_ENTRY']->value->get('sourcefield'));
$_smarty_tpl->_assignInScope('RECORD_TARGET_FIELD', $_smarty_tpl->tpl_vars['LISTVIEW_ENTRY']->value->get('targetfield'));?><span class="fa fa-pencil" onclick="javascript:Settings_PickListDependency_Js.triggerEdit(event, '<?php echo $_smarty_tpl->tpl_vars['RECORD_SOURCE_MODULE']->value;?>
', '<?php echo $_smarty_tpl->tpl_vars['RECORD_SOURCE_FIELD']->value;?>
', '<?php echo $_smarty_tpl->tpl_vars['RECORD_TARGET_FIELD']->value;?>
')" title="<?php echo vtranslate('LBL_EDIT',$_smarty_tpl->tpl_vars['MODULE']->value);?>
"></span><span class="fa fa-trash-o" onclick="javascript:Settings_PickListDependency_Js.triggerDelete(event, '<?php echo $_smarty_tpl->tpl_vars['RECORD_SOURCE_MODULE']->value;?>
', '<?php echo $_smarty_tpl->tpl_vars['RECORD_SOURCE_FIELD']->value;?>
', '<?php echo $_smarty_tpl->tpl_vars['RECORD_TARGET_FIELD']->value;?>
')" title="<?php echo vtranslate('LBL_DELETE',$_smarty_tpl->tpl_vars['MODULE']->value);?>
"></span></div><?php }
}
