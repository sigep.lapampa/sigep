<?php
/* Smarty version 3.1.39, created on 2023-02-08 19:19:02
  from 'C:\AppServ\www\vtigercrm75\layouts\v7\modules\RecycleBin\ListViewRecordActions.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_63e3f5a6126919_54428953',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '75a877ce8031e86be451e5c732d6756e2fef8401' => 
    array (
      0 => 'C:\\AppServ\\www\\vtigercrm75\\layouts\\v7\\modules\\RecycleBin\\ListViewRecordActions.tpl',
      1 => 1675544723,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_63e3f5a6126919_54428953 (Smarty_Internal_Template $_smarty_tpl) {
?><!--LIST VIEW RECORD ACTIONS--><div class="table-actions"><?php if (!$_smarty_tpl->tpl_vars['SEARCH_MODE_RESULTS']->value) {?><span class="input" ><input type="checkbox" value="<?php echo $_smarty_tpl->tpl_vars['LISTVIEW_ENTRY']->value->getId();?>
" class="listViewEntriesCheckBox"/></span><?php }?><span class="restoreRecordButton"><i title="<?php echo vtranslate('LBL_RESTORE',$_smarty_tpl->tpl_vars['MODULE']->value);?>
" class="fa fa-refresh alignMiddle"></i></span><span class="deleteRecordButton"><i title="<?php echo vtranslate('LBL_DELETE',$_smarty_tpl->tpl_vars['MODULE']->value);?>
" class="fa fa-trash alignMiddle"></i></span></div><?php }
}
