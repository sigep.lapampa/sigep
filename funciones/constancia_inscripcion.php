<?php 
date_default_timezone_set('America/Argentina/Buenos_Aires');
require_once('../libraries/fpdf184/fpdf.php');
require_once('conectar.php');

$sql = "INSERT INTO transaccion_certificado (id_transaccion_certificado,id_beneficiarios, usuario, fecha) VALUES ('','".$_GET['id']."','".$_GET['id_user']."','".date("Y/m/d")."')";	
$result = mysqli_query($link,$sql);

$sql = "SELECT MAX(id_transaccion_certificado) AS nro_transaccion_certificado FROM transaccion_certificado";	
$result = mysqli_query($link,$sql);
$fila = mysqli_fetch_array($result);
$nro_transaccion = $fila['nro_transaccion_certificado'];

$sql = "SELECT vtiger_beneficiarioscf.cf_1201, vtiger_beneficiarioscf.cf_1083, vtiger_beneficiarioscf.cf_1081, vtiger_beneficiarioscf.cf_1079, vtiger_beneficiarioscf.cf_1099, vtiger_beneficiarioscf.cf_1091,vtiger_crmentity.createdtime 
		FROM vtiger_beneficiarioscf INNER JOIN vtiger_crmentity ON vtiger_crmentity.crmid = vtiger_beneficiarioscf.beneficiariosid
		WHERE beneficiariosid = ".$_GET['id'];	
						
$result = mysqli_query($link,$sql);
$fila = mysqli_fetch_array($result);
	

$pdf = new FPDF();
$pdf->AddPage();
$pdf->SetFont('Times','',16);
//$pdf->Cell(40,10,'ID benef' . $_GET['id']);
$pdf->SetFont('Arial','B',18);
$pdf->Cell(100,30,"        Constancia de Inscripci�n",1,1);
$pdf->Ln(7);
$pdf->Cell(80,10,"        Fecha: ".date("d/m/y"));
$pdf->Ln(14);
$pdf->Cell(80,10,"Nro. transacci�n: " . $nro_transaccion);
$pdf->Cell(80,10,"Clave inscripci�n: ". $fila['cf_1099']);
$pdf->Ln(7);
$pdf->SetFont('Arial','B',14);
$pdf->Cell(80,10,"Apellido: ". $fila['cf_1083']);
$pdf->Cell(80,10,"Nombre: ". $fila['cf_1081']);
$pdf->Ln(7);
$pdf->SetFont('Arial','B',14);
$pdf->Cell(80,10,"DNI: ". $fila['cf_1079']);
$pdf->Cell(80,10,"Efrector: ".substr($fila['cf_1201'], 9));
$pdf->Ln(7);
$date = date_create($fila['createdtime']);
$pdf->SetFont('Arial','B',14);
$pdf->Cell(80,10,"Fecha inscripci�n: ". date_format($date,"d/m/Y"));
$date = date_create($fila['cf_1091']);
$pdf->Cell(80,10,"Fecha de Nacimiento: ". date_format($date,"d/m/Y"));

$pdf->Ln(14);
$pdf->SetFont('Arial','',10);
$pdf->cell(26,5,'Por la presente,');
$pdf->SetFont('Arial','B',10);
$pdf->cell(40,5,'SE DEJA CONSTANCIA');
$pdf->SetFont('Arial','',10);
$pdf->cell(95,5,' que la  persona antes descrita se encuentra inscripta en  el');
$pdf->SetFont('Arial','B',10);
$pdf->cell(28,5,'Programa Sumar');
$pdf->SetFont('Arial','',10);
$pdf->cell(38,5,' ,');
$pdf->Ln(7);
$pdf->cell(200,5,'y re�ne los requisitos necesarios para su validaci�n.');
$pdf->Ln(14);
$pdf->SetFont('Arial','B',10);
$pdf->cell(200,5,'CONSTANCIA V�LIDA PARA SER PRESENTADA ANTE ANSES.');


$pdf->SetFont('Arial','B',12);
$pdf->setxy(10,120);
$pdf->cell(190,9,"Pod�s comunicarte con la l�nea gratuita de informaci�n del Programa SUMAR: 0800-222-7100",0,0,'C');
$pdf->Ln(7);
$pdf->cell(190,9,"UIP/Sumar La Pampa. Ministerio de Salud.",0,0,'C');
$pdf->Ln(7);
$pdf->SetFont('Arial','',12);
$pdf->cell(190,9,"Bartolom� Mitre 131. Santa Rosa. CP:6300",0,0,'C');
$pdf->Ln(7);
$pdf->cell(190,9,"02954 - 424398",0,0,'C');
$pdf->Ln(6);
$pdf->cell(190,9,"Mail: programasumarlp@gmail.com",0,0,'C');

$pdf->Output();
?>
