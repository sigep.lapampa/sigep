<link rel="stylesheet" type="text/css" href="https://www.jeasyui.com/easyui/themes/default/easyui.css">
<link rel="stylesheet" type="text/css" href="https://www.jeasyui.com/easyui/themes/icon.css">
<link rel="stylesheet" type="text/css" href="https://www.jeasyui.com/easyui/themes/color.css">
<link rel="stylesheet" type="text/css" href="https://www.jeasyui.com/easyui/demo/demo.css">
<script type="text/javascript" src="https://code.jquery.com/jquery-1.9.1.min.js"></script>
<script type="text/javascript" src="https://www.jeasyui.com/easyui/jquery.easyui.min.js"></script> 
	
<script language="javascript" type="text/javascript">
var resultado;
var lat;
var lon;
function inicio(){
	$('#ajaxBusy').show();
	return '';
}
function fin(){
	$('#ajaxBusy').hide();
	return '';
}

</script>


<?php
include_once 'conectar.php';

//AUMENTAMOS EL TAMAÑO DE LOS ARCHIVOS PHP
ini_set('post_max_size', '30M');
ini_set('upload_max_filesize', '30M');
ini_set('max_execution_time', '1000');
ini_set('max_input_time', '1000');

$mostrar_form = true;
set_time_limit(0);

if (isset($_FILES['archivo_estado'])) { //Tenemos el archivo de estados
    $mostrar_form = false;
    //SI EL ARCHIVO SE ENVIA Y ADEMAS SE SUBIO CORRECTAMENTE
    if (isset($_FILES["archivo_estado"])) {
		
		?>
		<div class="ins_box_60p centrado_margin marg_15top redondeado3 fondo_gris">
			<div class="centrado_txt pad5">
				<div id="ajaxBusy">
					<BR>PROCESANDO ARCHIVO, ESPERE POR FAVOR... <BR><BR>
					<img src="wait.gif"><BR><BR>
				</div>
		    </div>
		</div>	
		<?php
		
		$errores = fopen("errores.txt", "w");
	
		//voy a crear un listado con los codigos sis y los cuie 
	$array_cuie = array(P03787 => '1', P03784 => '2', P03788 => '3', P03027 => '5', P03023 => '7', P03795 => '10', P03789 => '11', P06453 => '12', P03798 => '14', P03028 => '15',
						P01159 => '16', P06479 => '17', P03801 => '20', P03800 => '21', P03802 => '22', P03022 => '23', P03805 => '25', P06454 => '26', P03807 => '27', P03809 => '28',
						P03810 => '29', P06465 => '30', P03031 => '32', P03025 => '34', P06467 => '36', P03024 => '37', P03030 => '38', P06483 => '39', P03026 => '40', P03808 => '43',
						P03799 => '44', P03786 => '45', P06447 => '46', P06480 => '48', P01172 => '49', P03785 => '50', P03032 => '51', P03034 => '52', P03029 => '54', P03803 => '55',
						P06460 => '57', P06450 => '60', P06486 => '62', P06488 => '63', P06468 => '65', P06455 => '66', P03806 => '67', P01173 => '68', P01175 => '69', P06471 => '71',
						P03791 => '72', P03811 => '73', P01170 => '74', P06456 => '75', P03812 => '76', P03813 => '77', P03814 => '78', P06459 => '79', P06481 => '80', P01176 => '81',
						P01169 => '82', P06482 => '83', P01174 => '84', P06470 => '88', P06473 => '90', P06462 => '91', P01171 => '92', P06476 => '112', P01155 => '160', P01536 => '161',
						P01156 => '163', P01157 => '164', P03797 => '165', P01529 => '166', P03816 => '64', P06451 => '19', P06449 => '33', P06458 => '1062', P06446 => '61', P06477 => '70',
						P06475 => '41', P06474 => '89', P03804 => '24', P06461 => '47', P03033 => '18', P03796 => '13', P03794 => '8', P03792 => '53', P03790 => '6', P03793 => '9',
						P03815 => '35',P06496 => '85',P06452 => '120',P06494 => '162',P06495 => '167',P06493 => '168',P06497 => '97');

        //SE ABRE EL ARCHIVO EN MODO LECTURA
        $fp = fopen($_FILES['archivo_estado']['tmp_name'], "r");
        //SE RECORRE
        $nombre_tmp = $_FILES['archivo_estado']['tmp_name'];
        if (filesize($nombre_tmp) != 0) {
            // $nuevo_nombre = date("Y-m-d") . "-estados.csv";
            // $subir_a = "archivos_estados/$nuevo_nombre";
            // move_uploaded_file($nombre_tmp, $subir_a);
            $arr_datos = array();
            $sql = "";
            $cantidad_consultas = 0;
            $fecha_carga = date("Y-m-d");
            //$db->StartTrans();
            $arr_upd = array();
			
			//if(date("m") == "01"){$fecha_inicio = "01-12-". date("Y") - 1; /*$fecha_fin = "31-12-". date("Y") - 1; */ }
			if(date("m") == "01"){$fecha_inicio = "01-12-2019"; }
			if(date("m") == "02"){$fecha_inicio = "01-01-". date("Y"); /*$fecha_fin = "31-01-". date("Y");*/ }
			if(date("m") == "03"){$fecha_inicio = "01-02-". date("Y"); /*$fecha_fin = "28-02-". date("Y");*/ }
			if(date("m") == "04"){$fecha_inicio = "01-03-". date("Y"); /*$fecha_fin = "31-03-". date("Y");*/ }
			if(date("m") == "05"){$fecha_inicio = "01-04-". date("Y"); /*$fecha_fin = "30-04-". date("Y");*/ }
			if(date("m") == "06"){$fecha_inicio = "01-05-". date("Y"); /*$fecha_fin = "31-05-". date("Y");*/ }
			if(date("m") == "07"){$fecha_inicio = "01-06-". date("Y"); /*$fecha_fin = "30-06-". date("Y");*/ }
			if(date("m") == "08"){$fecha_inicio = "01-07-". date("Y"); /*$fecha_fin = "31-07-". date("Y");*/ }
			if(date("m") == "09"){$fecha_inicio = "01-08-". date("Y"); /*$fecha_fin = "31-08-". date("Y");*/ }
			if(date("m") == "10"){$fecha_inicio = "01-09-". date("Y"); /*$fecha_fin = "30-09-". date("Y");*/ }
			if(date("m") == "11"){$fecha_inicio = "01-10-". date("Y"); /*$fecha_fin = "31-10-". date("Y");*/ }
			if(date("m") == "12"){$fecha_inicio = "01-11-". date("Y"); /*$fecha_fin = "30-11-". date("Y");*/ }
			
			//grabar beneficiario
			//saco el id crm
			$sql = "SELECT * FROM vtiger_crmentity_seq";
			$result = mysqli_query($link,$sql);	
			$reg = mysqli_fetch_array($result);
			$crmid = $reg['id'];

			$sql = "SELECT * FROM vtiger_modtracker_basic_seq";
			$result = mysqli_query($link,$sql);	
			$reg = mysqli_fetch_array($result);
			$tracker_id = $reg['id'];

			$x=0;
            while (($datos = fgetcsv($fp, 1000, "\v")) !== FALSE) { //REARMAMOS LAS FILAS CON EL TAB
            //while (($datos = fgetcsv($fp, 1000, ",")) !== FALSE) { //REARMAMOS LAS FILAS CON LA COMA(
				$numero = count($datos);
               	for ($c = 0; $c < $numero; $c++) {//CREAMOS LAS COLUMNAS DEL ARREGLO
					
					$x++;
					$crmid++;
					$tracker_id++;

					$estado = $datos[$c];
					$arr_estado = explode(";", $datos[$c]);
					$str_estado = "";
					
					$id_beneficiario = $arr_estado[0];
					$estado_envio = $arr_estado[1];
					$clave_beneficiario = $arr_estado[2];
					$tipo_transaccion = $arr_estado[3];
					$apellido_benef = utf8_encode($arr_estado[4]);
					$nombre_benef = utf8_encode($arr_estado[5]);
					if($arr_estado[6] == 'P'){
						$clase_documento_benef = 'Propio';
					}else{
						$clase_documento_benef = 'Ajeno';
					}

					$tipo_documento = $arr_estado[7];
					$numero_doc = $arr_estado[8];
					$id_categoria = $arr_estado[9];
					$sexo = $arr_estado[10];
					$fecha_nacimiento_benef = $arr_estado[11];
					$provincia_nac = $arr_estado[12];
					$localidad_nac = $arr_estado[13];
					$pais_nac = $arr_estado[14];
					$indigena = $arr_estado[15];
					$id_tribu = $arr_estado[16];
					$id_lengua = $arr_estado[17];
					$alfabeta = $arr_estado[18];
					$estudios = $arr_estado[19];
					$anio_mayor_nivel = $arr_estado[20];
					
					if($arr_estado[22]<>''){

						$tipo_doc_responsable = $arr_estado[21];
						$nro_doc_responsable = $arr_estado[22];
						$apellido_responsable = utf8_encode($arr_estado[23]);
						$nombre_responsable = utf8_encode($arr_estado[24]);	
						$alfabeta_responsable = $arr_estado[25];	
						$estudios_responsable = $arr_estado[26];	
						$anio_mayor_nivel_responsable = $arr_estado[27];
						$estadoest_responsable = $arr_estado[74];	
						
					}
					
					if($arr_estado[29]<>''){

						$tipo_doc_responsable = $arr_estado[28];
						$nro_doc_responsable = $arr_estado[29];
						$apellido_responsable = utf8_encode($arr_estado[30]);
						$nombre_responsable = utf8_encode($arr_estado[31]);	
						$alfabeta_responsable = $arr_estado[32];	
						$estudios_responsable = $arr_estado[33];	
						$anio_mayor_nivel_responsable = $arr_estado[34];
						$estadoest_responsable = $arr_estado[82];	
						
					}

					if($arr_estado[36]<>''){

						$tipo_doc_responsable = $arr_estado[35];
						$nro_doc_responsable = $arr_estado[36];
						$apellido_responsable = utf8_encode($arr_estado[37]);
						$nombre_responsable = utf8_encode($arr_estado[38]);	
						$alfabeta_responsable = $arr_estado[39];	
						$estudios_responsable = $arr_estado[40];	
						$anio_mayor_nivel_responsable = $arr_estado[41];	
						$estadoest_responsable = $arr_estado[83];

					}

					$fecha_diagnostico_embarazo = $arr_estado[42];
					$semanas_embarazo = $arr_estado[43];
					$fecha_probable_parto = $arr_estado[44];
					$fecha_efectiva_parto = $arr_estado[45];
					$cuie_ea = $arr_estado[46];
					$cuie_ah = $arr_estado[47];
					$menor_convive_con_adulto = $arr_estado[48];
					$calle = utf8_encode($arr_estado[49]);
					$numero_calle = $arr_estado[50];
					$piso = $arr_estado[51];
					$dpto = $arr_estado[52];
					$manzana = $arr_estado[53];
					$entre_calle_1 = $arr_estado[54];
					$entre_calle_2 = $arr_estado[55];
					$telefono = $arr_estado[56];
					$departamento = utf8_encode($arr_estado[57]);
					$localidad = utf8_encode($arr_estado[58]);
					$municipio = utf8_encode($arr_estado[59]);
					$barrio = utf8_encode($arr_estado[60]);
					$cod_pos = $arr_estado[61];

					$observaciones = utf8_encode($arr_estado[62]);
					$fecha_inscripcion = $arr_estado[63];
					$fecha_carga = $arr_estado[64];
					$usuario_carga = $arr_estado[65];
					$activo = $arr_estado[66];
					$score_riesgo = $arr_estado[67];
					$mail = $arr_estado[68];
					$celular = $arr_estado[69];
					$otrotel = $arr_estado[70];
					$estadoest = $arr_estado[71];
					$fum = $arr_estado[72];
					$obsgenerales = utf8_encode($arr_estado[73]);
					$tipo_ficha = $arr_estado[75];
					$responsable = $arr_estado[76];
					$discv = $arr_estado[77];
					$disca = $arr_estado[78];
					$discmo = $arr_estado[79];
					$discme = $arr_estado[80];
					$otradisc = $arr_estado[81];
					$menor_embarazada = $arr_estado[84];
					$campos_modificados = $arr_estado[85];
					$latitud = $arr_estado[86];
					$longitud = $arr_estado[87];
					$cuie_efector_acargo = $arr_estado[88];
					$id_mpi = $arr_estado[89];
					$id_equipo_nuclear = $arr_estado[90];
					
						$sql = "INSERT INTO vtiger_crmentity(crmid, smcreatorid, smownerid, modifiedby, setype, description, createdtime, modifiedtime, viewedtime, status, version, presence, deleted, smgroupid, source, label) 
														VALUES ('$crmid','1','1','1','Beneficiarios','','$fecha_inscripcion','$fecha_inscripcion','','','0','1','0','0','Migracion','')";
						$result = mysqli_query($link,$sql);
						$sql = "INSERT INTO vtiger_crmentity_user_field(recordid, userid, starred) VALUES ('$crmid','1','0')";
						$result = mysqli_query($link,$sql);
						

						$sql = "INSERT INTO vtiger_beneficiarios(Beneficiariosid) VALUES ('$crmid')";
						$result = mysqli_query($link,$sql);	
						
						$sql = "SELECT cf_1011  FROM vtiger_cf_1011 WHERE cf_1011 LIKE '%".$cuie_ea."%'";
						$result = mysqli_query($link,$sql);	
						$reg = mysqli_fetch_array($result);
						$cuie = $reg['cf_1011'];

						$sql = "INSERT INTO vtiger_beneficiarioscf(`beneficiariosid`, `cf_1075`, `cf_1077`, `cf_1079`, `cf_1081`, `cf_1083`, `cf_1085`, `cf_1087`, `cf_1273`, `cf_1091`, `cf_1093`, `cf_1095`, `cf_1097`, `cf_1099`, `cf_1101`, `cf_1103`, `cf_1105`, `cf_1107`, `cf_1109`, `cf_1111`, `cf_1113`, `cf_1115`, `cf_1117`, `cf_1119`, `cf_1121`, `cf_1123`, `cf_1125`, `cf_1127`, `cf_1129`, `cf_1131`, `cf_1133`, `cf_1135`, `cf_1137`, `cf_1139`, `cf_1141`, `cf_1143`, `cf_1145`, `cf_1147`, `cf_1149`, `cf_1151`, `cf_1153`, `cf_1155`, `cf_1157`, `cf_1159`, `cf_1161`, `cf_1163`, `cf_1165`, `cf_1167`, `cf_1169`, `cf_1171`, `cf_1173`, `cf_1175`, `cf_1177`, `cf_1179`, `cf_1181`, `cf_1183`, `cf_1185`, `cf_1187`, `cf_1189`, `cf_1191`, `cf_1193`, `cf_1195`, `cf_1197`, `cf_1199`, `cf_1201`, `cf_1203`, `cf_1205`, `cf_1207`, `cf_1209`, `cf_1211`, `cf_1213`, `cf_1215`, `cf_1217`, `cf_1219`, `cf_1221`, `cf_1223`, `cf_1225`, `cf_1227`) VALUES ('$crmid','$clase_documento_benef','$tipo_documento','$numero_doc','$nombre_benef','$apellido_benef','$mail','$celular','$sexo','$fecha_nacimiento_benef','$pais_nac','$provincia_nac','$id_beneficiario','$clave_beneficiario','$cuie','','$estado_envio','$tipo_transaccion','$id_categoria','$menor_convive_con_adulto','$calle','$numero_calle','$piso','$dpto','$manzana','$entre_calle_1','$entre_calle_2','$telefono','$localidad','$departamento','$cod_pos','$municipio','$barrio','$latitud','$longitud','$observaciones','$menor_embarazada','$fum','$fecha_diagnostico_embarazo','$semanas_embarazo','$fecha_probable_parto','$fecha_efectiva_parto','$indigena','$id_tribu','$id_lengua','$alfabeta','$estadoest','$estudios','$anio_mayor_nivel','$responsable','$tipo_doc_responsable','$nro_doc_responsable','$apellido_responsable','$nombre_responsable','$alfabeta_responsable','$estadoest_responsable','$estudios_responsable','$anio_mayor_nivel_responsable','$score_riesgo','$discv','$disca','$discmo','$discme','$otradisc','$cuie','$obsgenerales','','','','','','','','','','$id_mpi','$id_equipo_nuclear','$tipo_ficha')";

						$result = mysqli_query($link,$sql);

						$sql = "INSERT INTO vtiger_customerdetails(customerid) VALUES ('$crmid')";
						$result = mysqli_query($link,$sql);
						
						$sql = "INSERT INTO vtiger_modtracker_basic(id, crmid, module, whodid, changedon, status) 
						VALUES ('$tracker_id','$crmid','Beneficiarios','1', CURDATE(),'2')";
						$result = mysqli_query($link,$sql);

						$tracker_id++;
						$sql = "INSERT INTO vtiger_modtracker_basic(id, crmid, module, whodid, changedon, status) 
						VALUES ('$tracker_id','$crmid','Beneficiarios','1', CURDATE(),'4')";
						$result = mysqli_query($link,$sql);

						$cantidad_consultas++; 

				}

				//grabo el proximo crmid
				$sql = "UPDATE vtiger_crmentity_seq SET id='$crmid'";
				$result = mysqli_query($link,$sql);

				$sql = "UPDATE vtiger_modtracker_basic_seq SET id='$tracker_id'";
				$result = mysqli_query($link,$sql);

				$msj_aviso = "FIN DEL PROCESO. Cantidad de registros <strong>$x</strong> consultas. <br>";
				$msj_aviso .= "Se ejecutaron correctamente <strong>$cantidad_consultas</strong> consultas. <br>";
				$msj_aviso .= "Se ejecutaron con errores <strong>$cantidad_consultas_error</strong> consultas.";
			}
			fclose($errores);
			$var_php = "<script>document.writeln(fin());</script>";
			echo $var_php;
			
		}
	}else{
		echo "Error de subida";
	}
}
?>
<script>
    $(document).ready(function () {
        $("#btnExport").click(function (e) {
            window.open('data:application/vnd.ms-excel,' + encodeURIComponent($('#benef_no_estado').html()));
            e.preventDefault();
        });
    });
	
</script>
<style>
    .ins_box_60p {
        width: 60%;
    }

    .marg_15top {
        margin-top: 15px;
    }
    .centrado_margin {
        margin: 0 auto;
    }
    .centrado_txt {
        text-align:  center;
    }
    .redondeado3 {
        border: 1px solid #cccccc;
        border-radius: 3px;
        -webkit-border-radius: 3px 3px 3px;
    }

    .borde_gris {
        border: 1px solid #b3b3b3;
    }

    .fondo_gris {
        background-color: #f1f1f1;
    }

    .pad5 {
        padding: 5px;
    }

    .marg5 {
        margin: 5px;
    }
</style>
<div class="ins_box_60p centrado_margin marg_15top redondeado3 fondo_gris">
	
		
	<div class="centrado_txt pad5">
		<div id="ajaxBusy" style="display:none;">
			<BR>PROCESANDO ARCHIVO, ESPERE POR FAVOR... <BR><BR>
			<img src="wait.gif"><BR><BR>
		</div>
    </div>

	
    <?php
    if ($mostrar_form) {
        ?>
        <h3>IMPORTAR TODOS LOS BENEFICIARIOS</h3>
        <form action="importar_beneficiarios.php" method="POST" enctype="multipart/form-data">
            <table style="margin: 0 auto; margin-top: 10px;" class="marg5">
                <tr>
                    <td>
                        <input type="file" name="archivo_estado" id="archivo_estado"/>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" class="centrado_txt">
                        <div class="marg5">
                            <input type="submit" name="boton_submit" value="Cargar" onclick="$('#ajaxBusy').show();">
                        </div>
                    </td>
                </tr>
            </table>
            <hr style="background-color: gray; height: 1px; border: 0;"/>
        </form>
    <?php } else { ?>
        <div class="centrado_txt pad5">
            <?php echo $msj_aviso ?>
        </div>
    <?php } ?>
</div>