<link rel="stylesheet" type="text/css" href="https://www.jeasyui.com/easyui/themes/default/easyui.css">
<link rel="stylesheet" type="text/css" href="https://www.jeasyui.com/easyui/themes/icon.css">
<link rel="stylesheet" type="text/css" href="https://www.jeasyui.com/easyui/themes/color.css">
<link rel="stylesheet" type="text/css" href="https://www.jeasyui.com/easyui/demo/demo.css">
<script type="text/javascript" src="https://code.jquery.com/jquery-1.9.1.min.js"></script>
<script type="text/javascript" src="https://www.jeasyui.com/easyui/jquery.easyui.min.js"></script> 
	
<script language="javascript" type="text/javascript">
var resultado;
var lat;
var lon;
function inicio(){
	$('#ajaxBusy').show();
	return '';
}
function fin(){
	$('#ajaxBusy').hide();
	return '';
}

</script>


<?php
include_once 'conectar.php';

//AUMENTAMOS EL TAMAÑO DE LOS ARCHIVOS PHP
ini_set('post_max_size', '30M');
ini_set('upload_max_filesize', '30M');
ini_set('max_execution_time', '1000');
ini_set('max_input_time', '1000');

$mostrar_form = true;
set_time_limit(0);

if (isset($_FILES['archivo_estado'])) { //Tenemos el archivo de estados
    $mostrar_form = false;
    //SI EL ARCHIVO SE ENVIA Y ADEMAS SE SUBIO CORRECTAMENTE
    if (isset($_FILES["archivo_estado"])) {
		
		?>
		<div class="ins_box_60p centrado_margin marg_15top redondeado3 fondo_gris">
			<div class="centrado_txt pad5">
				<div id="ajaxBusy">
					<BR>PROCESANDO ARCHIVO, ESPERE POR FAVOR... <BR><BR>
					<img src="wait.gif"><BR><BR>
				</div>
		    </div>
		</div>	
		<?php
		
		$errores = fopen("errores.txt", "w");
	
		//voy a crear un listado con los codigos sis y los cuie 
	$array_cuie = array(P03787 => '1', P03784 => '2', P03788 => '3', P03027 => '5', P03023 => '7', P03795 => '10', P03789 => '11', P06453 => '12', P03798 => '14', P03028 => '15',
						P01159 => '16', P06479 => '17', P03801 => '20', P03800 => '21', P03802 => '22', P03022 => '23', P03805 => '25', P06454 => '26', P03807 => '27', P03809 => '28',
						P03810 => '29', P06465 => '30', P03031 => '32', P03025 => '34', P06467 => '36', P03024 => '37', P03030 => '38', P06483 => '39', P03026 => '40', P03808 => '43',
						P03799 => '44', P03786 => '45', P06447 => '46', P06480 => '48', P01172 => '49', P03785 => '50', P03032 => '51', P03034 => '52', P03029 => '54', P03803 => '55',
						P06460 => '57', P06450 => '60', P06486 => '62', P06488 => '63', P06468 => '65', P06455 => '66', P03806 => '67', P01173 => '68', P01175 => '69', P06471 => '71',
						P03791 => '72', P03811 => '73', P01170 => '74', P06456 => '75', P03812 => '76', P03813 => '77', P03814 => '78', P06459 => '79', P06481 => '80', P01176 => '81',
						P01169 => '82', P06482 => '83', P01174 => '84', P06470 => '88', P06473 => '90', P06462 => '91', P01171 => '92', P06476 => '112', P01155 => '160', P01536 => '161',
						P01156 => '163', P01157 => '164', P03797 => '165', P01529 => '166', P03816 => '64', P06451 => '19', P06449 => '33', P06458 => '1062', P06446 => '61', P06477 => '70',
						P06475 => '41', P06474 => '89', P03804 => '24', P06461 => '47', P03033 => '18', P03796 => '13', P03794 => '8', P03792 => '53', P03790 => '6', P03793 => '9',
						P03815 => '35',P06496 => '85',P06452 => '120',P06494 => '162',P06495 => '167',P06493 => '168',P06497 => '97');

        //SE ABRE EL ARCHIVO EN MODO LECTURA
        $fp = fopen($_FILES['archivo_estado']['tmp_name'], "r");
        //SE RECORRE
        $nombre_tmp = $_FILES['archivo_estado']['tmp_name'];
        if (filesize($nombre_tmp) != 0) {
            // $nuevo_nombre = date("Y-m-d") . "-estados.csv";
            // $subir_a = "archivos_estados/$nuevo_nombre";
            // move_uploaded_file($nombre_tmp, $subir_a);
            $arr_datos = array();
            $sql = "";
            $cantidad_consultas = 0;
            $fecha_carga = date("Y-m-d");
            //$db->StartTrans();
            $arr_upd = array();
			
			//if(date("m") == "01"){$fecha_inicio = "01-12-". date("Y") - 1; /*$fecha_fin = "31-12-". date("Y") - 1; */ }
			if(date("m") == "01"){$fecha_inicio = "01-12-2019"; }
			if(date("m") == "02"){$fecha_inicio = "01-01-". date("Y"); /*$fecha_fin = "31-01-". date("Y");*/ }
			if(date("m") == "03"){$fecha_inicio = "01-02-". date("Y"); /*$fecha_fin = "28-02-". date("Y");*/ }
			if(date("m") == "04"){$fecha_inicio = "01-03-". date("Y"); /*$fecha_fin = "31-03-". date("Y");*/ }
			if(date("m") == "05"){$fecha_inicio = "01-04-". date("Y"); /*$fecha_fin = "30-04-". date("Y");*/ }
			if(date("m") == "06"){$fecha_inicio = "01-05-". date("Y"); /*$fecha_fin = "31-05-". date("Y");*/ }
			if(date("m") == "07"){$fecha_inicio = "01-06-". date("Y"); /*$fecha_fin = "30-06-". date("Y");*/ }
			if(date("m") == "08"){$fecha_inicio = "01-07-". date("Y"); /*$fecha_fin = "31-07-". date("Y");*/ }
			if(date("m") == "09"){$fecha_inicio = "01-08-". date("Y"); /*$fecha_fin = "31-08-". date("Y");*/ }
			if(date("m") == "10"){$fecha_inicio = "01-09-". date("Y"); /*$fecha_fin = "30-09-". date("Y");*/ }
			if(date("m") == "11"){$fecha_inicio = "01-10-". date("Y"); /*$fecha_fin = "31-10-". date("Y");*/ }
			if(date("m") == "12"){$fecha_inicio = "01-11-". date("Y"); /*$fecha_fin = "30-11-". date("Y");*/ }
			
			$x=0;
            while (($datos = fgetcsv($fp, 1000, "\v")) !== FALSE) { //REARMAMOS LAS FILAS CON EL TAB
            //while (($datos = fgetcsv($fp, 1000, ",")) !== FALSE) { //REARMAMOS LAS FILAS CON LA COMA(
				$numero = count($datos);
               	for ($c = 0; $c < $numero; $c++) {//CREAMOS LAS COLUMNAS DEL ARREGLO
					
					$x++;
					
					$estado = $datos[$c];
					$arr_estado = explode(";", $datos[$c]);
					$str_estado = "";
					
					$control = true;
					$control2 = true;

					$fila_error = $cantidad_consultas + $cantidad_consultas_error + 1;
					
					if($arr_estado[3] == ""){
						$arr_estado[3] = "P";
					}
					if(($arr_estado[3] == 'P') or ($arr_estado[3] == 'A')){
						if($arr_estado[3] == 'P'){
							$clase_documento_benef = 'PROPIO';
						}else{
							$clase_documento_benef = 'AJENO';
						}
					}else{
						fputs($errores, "Error en clase documento beneficiario, (fila $fila_error)." . PHP_EOL);
						
						$control = false;
					}

					if($arr_estado[4] == ""){
						$arr_estado[4] = "DNI";
					}	
					if(($arr_estado[4] == 'DNI') or ($arr_estado[4] == 'LC') or ($arr_estado[4] == 'LE') or ($arr_estado[4] == 'HEX') or ($arr_estado[4] == 'PAS')){
						$tipo_documento = strtoupper($arr_estado[4]);
					}else{
						
						fputs($errores, "Error en tipo documento, (fila $fila_error)." . PHP_EOL);
						$control = false;
					}					
					
					if($arr_estado[5] != ''){
						$numero_doc = $arr_estado[5];
						if(($numero_doc >= 80000000) and ($numero_doc < 90000000)){
							fputs($errores, "Error - documento fuera de rango, (fila $fila_error)." . PHP_EOL);
							$control = false;
						}
					}else{
						
						fputs($errores, "Error en documento, (fila $fila_error)." . PHP_EOL);
						$control = false;
					}					

					$sql_benef = "SELECT beneficiariosid FROM vtiger_beneficiarioscf WHERE (cf_1075='$clase_documento_benef') 
																  AND (cf_1077='$tipo_documento') 
																  AND (cf_1079='$numero_doc')";
					//$result_benef = @pg_query($sql_benef);
					$result_benef = mysqli_query($link,$sql_benef);
					
					if(mysqli_num_rows($result_benef)){
						fputs($errores, "Ya existe el beneficiario, (fila $fila_error)." . PHP_EOL);
						$control = false;
						
						
					}else{
						if($arr_estado[0] == 'A'){
							$tipo_transaccion = $arr_estado[0];
						}else{
							fputs($errores, "El tipo de transaccion es distinto de A, (fila $fila_error)" . PHP_EOL);
							
							$control = false;
						}

						if($arr_estado[1] != ""){
								$apellido_benef = strtoupper(preg_replace("[^A-Za-z0-9' ']", "", $arr_estado[1]));
						}else{
							
							fputs($errores, "apellido vacio, (fila $fila_error)." . PHP_EOL);
							$control = false;
						}
						if($arr_estado[2] != ""){
							$nombre_benef = strtoupper(preg_replace("[^A-Za-z0-9' ']", "", $arr_estado[2]));
						}else{
							fputs($errores, "nombre vacio, (fila $fila_error)." . PHP_EOL);
							
							$control = false;
						}
						
						$celular = $arr_estado[6];
						
						if(($arr_estado[8] == 'F') or ($arr_estado[8] == 'M')){
							$sexo = strtoupper($arr_estado[8]);
						}else{
							fputs($errores, "Error sexo, (fila $fila_error)." . PHP_EOL);
							
							$control = false;
						}
						
						if($arr_estado[9] != ''){
							$partes= explode("/", $arr_estado[9]); 
							if (checkdate ($partes[1],$partes[0],$partes[2])) {
								$fecha_nacimiento = $partes[2] . "-".$partes[1]."-".$partes[0];
							}else{
								
								fputs($errores, "Error en fecha nacimiento, (fila $fila_error)." . PHP_EOL);
								$control = false;
							}
						}else{
							fputs($errores, "Error fecha nacimiento vacia, (fila $fila_error)." . PHP_EOL);
							
							$control = false;
						}
						
						if($control){
							
							$ape = strtoupper($apellido_benef);
							$nom = strtoupper($nombre_benef);
							
							$ape = explode(" ",$ape);//corto el apellido 
							$ape1 = $ape[0]; // primera palabra del apellido completo
							
							$nom = explode(" ",$nom);//corto el nombre 
							$nom1 = $nom[0]; // primera palabra del nombre completo
							
							$sql_benef = "SELECT beneficiariosid FROM vtiger_beneficiarioscf WHERE (CF_1081 LIKE '%$nom1%') AND (cf_1091='$fecha_nacimiento')";
							
							$result_benef = mysqli_query($link,$sql_benef);
							$resultado = mysqli_fetch_array($result_benef);
							if(mysqli_num_rows($result_benef)){
								fputs($errores, "fecha de nacimiento igual - PENDIENTE DE CONTROL - datos personales similares(nombre), (dni: $numero_doc - $ape1  $nom1 - fila $fila_error)." . PHP_EOL);
							}
							
							$sql_benef = "SELECT beneficiariosid FROM vtiger_beneficiarioscf WHERE (cf_1083 LIKE '%$ape1%') AND (cf_1091='$fecha_nacimiento')";

							//$result_benef = pg_query($sql_benef);
							$result_benef = mysqli_query($link,$sql_benef);
							$resultado = mysqli_fetch_array($result_benef);
							if(mysqli_num_rows($result_benef)){
								fputs($errores, "Error 1. fecha de nacimiento igual - PENDIENTE DE CONTROL - datos personales similares, (dni: $numero_doc - $ape1  $nom1 - fila $fila_error)." . PHP_EOL);
								//$control = false;
							
							}else{
								
								$ape1 = $ape[0]; // primera palabra del apellido completo
								$nom1 = $nom[1]; // segunda palabra del nombre completo
								
								if($nom1 != ""){
									
									$sql_benef = "SELECT beneficiariosid FROM vtiger_beneficiarioscf WHERE (CF_1081 LIKE '%$nom1%') AND (cf_1091='$fecha_nacimiento')";

									$result_benef = mysqli_query($link,$sql_benef);
									$resultado = mysqli_fetch_array($result_benef);
									if(mysqli_num_rows($result_benef)){
										fputs($errores, "fecha de nacimiento igual - datos personales similares(nombre), (dni: $numero_doc - $ape1  $nom1 - fila $fila_error)." . PHP_EOL);
									}
									
									$sql_benef = "SELECT beneficiariosid FROM vtiger_beneficiarioscf WHERE (cf_1083 LIKE '%$ape1%') AND (cf_1091='$fecha_nacimiento')";

									$result_benef = mysqli_query($link,$sql_benef);
									$resultado = mysqli_fetch_array($result_benef);
									if(mysqli_num_rows($result_benef)){
										fputs($errores, "Error 2. fecha de nacimiento igual - datos personales similares, (dni: $numero_doc - $ape1  $nom1 - fila $fila_error)." . PHP_EOL);
										$control = false;
									}else{
										
										$ape1 = $ape[1]; // segunda palabra del apellido completo
										$nom1 = $nom[0]; // primera palabra del nombre completo
										if($ape1 != ""){	
										
											$sql_benef = "SELECT beneficiariosid FROM vtiger_beneficiarioscf WHERE (cf_1083 LIKE '%$ape1%') AND (cf_1091='$fecha_nacimiento')";

											$result_benef = mysqli_query($link,$sql_benef);
											$resultado = mysqli_fetch_array($result_benef);
											if(mysqli_num_rows($result_benef)){
												fputs($errores, "Error 3. fecha de nacimiento igual - datos personales similares, (dni: $numero_doc - $ape1  $nom1 - fila $fila_error)." . PHP_EOL);
												$control = false;
											}else{
												
												$ape1 = $ape[1]; // segunda palabra del apellido completo
												$nom1 = $nom[1]; // segunda palabra del nombre completo
												
												$sql_benef = "SELECT beneficiariosid FROM vtiger_beneficiarioscf WHERE (cf_1083 LIKE '%$ape1%') AND (cf_1091='$fecha_nacimiento')";

												$result_benef = mysqli_query($link,$sql_benef);
												$resultado = mysqli_fetch_array($result_benef);
												if(mysqli_num_rows($result_benef)){
													fputs($errores, "Error 4. fecha de nacimiento igual - datos personales similares, (dni: $numero_doc - $ape1  $nom1 - fila $fila_error)." . PHP_EOL);
													$control = false;
												}
											}
										}
									}
									
									
								}
								
								
							}
						}
						
						
						$pais = "ARGENTINA";
						//$menor_convive = 'S';

						if($arr_estado[12] != ""){
							$calle = strtoupper($arr_estado[12]);
						}else{
							fputs($errores, "calle vacio, (fila $fila_error)." . PHP_EOL);
							
							$control = false;
						}

						if(($arr_estado[13] != "") or ($arr_estado[14] != "") or ($arr_estado[15] != "") or ($arr_estado[16] != "") or ($arr_estado[17] != "")){
							$puerta = $arr_estado[13];
							$depto = strtoupper($arr_estado[14]);
							$departamento = strtoupper($arr_estado[15]);
							$municipio = strtoupper($arr_estado[16]);
							$localidad = strtoupper($arr_estado[17]);
							
							
							//aca voy a preguntar si existe la localidad enviada, sino existe la grabo igual pero dejo un registro para luego poder cambiarla
							$sql_loc= "SELECT cf_1129 FROM  vtiger_cf_1129 WHERE (cf_1129='$localidad')";
							$result_loc = mysqli_query($link,$sql_loc);
							if(!(mysqli_num_rows($result_loc))){
								fputs($errores, "Verificar LOCALIDAD, DNI: $numero_doc - $apellido_benef (fila $fila_error)." . PHP_EOL);
							}
							
						}else{
							fputs($errores, "Datos domicilio incompletos, (fila $fila_error)." . PHP_EOL);
							$control2 = false; //control2: esto lo hago porque despues tengo que controlar cuando le asigno santa rosa al cuie
						}

						if(is_numeric($arr_estado[18])){

							$buscar   = '.';
							$codigo_postal = $arr_estado[18];
							$pos = strpos($codigo_postal, $buscar);
							if ($pos === false) {

							} else {
								$codigo_postal = str_replace ('.','',$codigo_postal);
								//echo "CP: $codigo_postal <br> ";
							}
							
						}else{
							fputs($errores, "Error codigo postal, (fila $fila_error)." . PHP_EOL);
							
							$control = false;
						}

						$apellido_madre = strtoupper(preg_replace("[^A-Za-z0-9' ']", "", $arr_estado[19]));
						$nombre_madre = strtoupper(preg_replace("[^A-Za-z0-9' ']", "", $arr_estado[20]));
						
						$fecha = time() - strtotime($fecha_nacimiento);
						$edad = floor((($fecha / 3600) / 24) / 360);
						// echo '<p>La edad es '.$edad.' años</p>';

						
						if($arr_estado[21] == ""){
							$arr_estado[21] = "DNI";
						}						
						if(($arr_estado[21] == 'DNI') or ($arr_estado[21] == 'LC') or ($arr_estado[21] == 'LE') or ($arr_estado[21] == 'HEX') or ($arr_estado[21] == 'PAS')){
							$tipo_doc_madre = strtoupper($arr_estado[21]);
						}else{
							if($edad <= 14){
								fputs($errores, "Error tipo doc madre, (fila $fila_error)." . PHP_EOL);
								//$control = false;
							}		
						}
												
						if($arr_estado[22] != ""){
							$nro_doc_madre = $arr_estado[22];
						}else{
							if($edad <= 14){
								fputs($errores, "nro doc madre vacio, (fila $fila_error)." . PHP_EOL);
								//$control = false;
							}
						}
						
						if($arr_estado[23] != ''){
							$partes= explode("/", $arr_estado[23]); 
							if (checkdate($partes[1],$partes[0],$partes[2])) {
								$fecha_inscripcion = $partes[2] . "-".$partes[1]."-".$partes[0];
							}else{
								fputs($errores, "Error fecha inscripcion, (fila $fila_error)." . PHP_EOL);
								$control = false;
							}
						}else{
							
							$fecha_inicio2 = strtotime($fecha_inicio);
							$fecha_hoy = date("d-m-Y");
							$fecha_fin2 = strtotime($fecha_hoy);
							
							$partes_nac= explode("/", $arr_estado[9]); 
							$fecha_nac = $partes_nac[0] . "-".$partes_nac[1]."-".$partes_nac[2];
							$fecha_nac = strtotime($fecha_nac);
							
							if(($fecha_inicio2 < $fecha_nac) and ($fecha_nac < $fecha_fin2)){
									$fecha_inscripcion = $fecha_nacimiento;
							}else{
									$part= explode("-", $fecha_inicio);
									$fecha_inscripcion = $part[2] . "-".$part[1]."-".$part[0];
							}
							
						}					
						
						$arr_estado[24] = str_replace(' ', '', $arr_estado[24]);
						if($arr_estado[24] != ""){

							$codigo_sis = $arr_estado[24];
							$cuie = array_search($codigo_sis, $array_cuie); 
							
							$sql_cp= "SELECT cuidad, cuie FROM efe_conv WHERE (cod_pos='$codigo_postal')";
							$result_cp = mysqli_query($link,$sql_cp);							
							
							if(!(array_search($codigo_sis, $array_cuie))){	
								if(mysqli_num_rows($result_cp)){
									
									$reg_cuie = mysqli_fetch_array($result_cp);
									$cuie = $reg_cuie[cuie];
									fputs($errores, "CUIE asignado por CP - ver, (fila $fila_error)." . PHP_EOL); 
									
									if($control2 == false) {
										$municipio = $reg_cuie[cuidad];
										$localidad = $reg_cuie[cuidad];
										$control2 = true;
									}
									
								}else{
									
									$cuie = "P06447";
									if($control2 == false) {
										$municipio = "SANTA ROSA";
										$localidad = "SANTA ROSA";
										$control2 = true;
									}
									
									fputs($errores, "CUIE asignado P06447, (fila $fila_error)." . PHP_EOL); 								
									//$control = false;
								}
							}
							
						}else{
							
							$sql_cp= "SELECT cuidad, cuie FROM efe_conv WHERE (cod_pos='$codigo_postal')";
							$result_cp = mysqli_query($link,$sql_cp);							
							
							if(mysqli_num_rows($result_cp)){
								
								$reg_cuie = mysqli_fetch_array($result_cp);
								$cuie = $reg_cuie[cuie];
								fputs($errores, "CUIE asignado por CP - ver, (fila $fila_error)." . PHP_EOL); 	
								
								if($control2 == false) {
										$municipio = $reg_cuie[cuidad];
										$localidad = $reg_cuie[cuidad];
										$control2 = true;
									}
									
							}else{
								
								$cuie = "P06447";
								if($control2 == false) {
										$municipio = "SANTA ROSA";
										$localidad = "SANTA ROSA";
										$control2 = true;
									}								
								fputs($errores, "CUIE asignado P06447, (fila $fila_error)." . PHP_EOL); 
								//$control = false;								
							}
							
						}
					}
					
					// voy a controlar que este beneficiario no tenga otro con el mismo nombre,
					// mismo apellido, misma fecha de naciomiento y mismo dni de la madre
					$sql_rep = "";
					$sql_rep = "SELECT cf_1099 FROM vtiger_beneficiarioscf
													  WHERE (cf_1091 ='".$fecha_nacimiento."')
													    AND (cf_1081 = '".$nombre_benef."')
														AND (cf_1083 = '".$apellido_benef."')
														AND (cf_1175 = '".$nro_doc_madre."')";
											
					$result_rep = mysqli_query($link,$sql_rep);	
					
					$control3=true;//por defecto lo pongo verdadero y si se repite el benef lo cambio a false
					
					if(mysqli_num_rows($result_rep)){
						
						$arr = mysqli_fetch_array($result_rep);
						$id_b = $arr['cf_1099'];
						fputs($errores, "benef repetido (mismo nombre - apellido - fecha nac y dni madre), (id: $id_b fila $fila_error)." . PHP_EOL);
						$control3=false;
					}								
						
					//si no hay ningun error control en verdadero y si no control en falso y muestra el error
					if(($control) and ($control2) and ($control3)){
						
						//grabar beneficiario
						//saco el id crm
						$sql = "SELECT * FROM vtiger_crmentity_seq";
						$result = mysqli_query($link,$sql);	
						$reg = mysqli_fetch_array($result);
						$crmid = $reg['id'] + 1;
						
						$sql = "INSERT INTO vtiger_crmentity(crmid, smcreatorid, smownerid, modifiedby, setype, description, createdtime, modifiedtime, viewedtime, status, version, presence, deleted, smgroupid, source, label) 
														VALUES ('$crmid','1','1','1','Beneficiarios','','$fecha_inscripcion','$fecha_inscripcion','','','0','1','0','0','Alt.Aut.','')";
						$result = mysqli_query($link,$sql);

						$sql = "INSERT INTO vtiger_crmentity_user_field(recordid, userid, starred) VALUES ('$crmid','1','0')";
						$result = mysqli_query($link,$sql);


						$sql = "INSERT INTO vtiger_beneficiarios(beneficiariosid, beneficiariosno, beneficiarios_tks_dato, tags) VALUES ('$crmid','$crmid','','')";

						$id_beneficiario calcular
						$estado_envio = 'N';
						$tipo_transaccion = 'A';
						$id_categoria ver
						$menor_convive ver
						$numero_calle = $puerta;
						$piso = '';

						$sql = "INSERT INTO vtiger_beneficiarioscf(beneficiariosid, cf_1075, cf_1077, cf_1079, cf_1081, cf_1083, cf_1085, cf_1087, cf_1091, cf_1093, cf_1095, cf_1097, cf_1099, cf_1101, cf_1103, cf_1105, cf_1107, cf_1109, cf_1111, cf_1113, cf_1115, cf_1117, cf_1119 cf_1121, cf_1123, cf_1125, cf_1127, cf_1129, cf_1131, cf_1135, cf_1137, cf_1139, cf_1141, cf_1143, cf_1145, cf_1147, cf_1149, cf_1151, cf_1153, cf_1155, cf_1157, cf_1159, cf_1161, cf_1163, cf_1165, cf_1167, cf_1169, cf_1171, cf_1173, cf_1175, cf_1177, cf_1179, cf_1181, cf_1183, cf_1185, cf_1187, cf_1189, cf_1191, cf_1193, cf_1195, cf_1197, cf_1199, cf_1201, cf_1203, cf_1205, cf_1207, cf_1209, cf_1211, cf_1213, cf_1215, cf_1217, cf_1219, cf_1221, cf_1223, cf_1225, cf_1227, cf_1263, cf_1265, cf_1267, cf_1269, cf_1271, cf_1273) VALUES (`$crmid`, `$clase_documento_benef`, `$tipo_documento`, `$numero_doc`, `$nombre_benef`, `$apellido_benef`, `$mail`, `$celular`, `$fecha_nacimiento`, `$pais`, ``, `$id_beneficiario`, `$clave_beneficiario`, `$cuie`, `$edad`, `$estado_envio`, `$tipo_transaccion`, `$id_categoria`, `$menor_convive`, `$calle`, `$numero_calle`, `$piso`, `$departamento`, `cf_1121`, `cf_1123`, `cf_1125`, `cf_1127`, `cf_1129`, `cf_1131`, `cf_1135`, `cf_1137`, `cf_1139`, `cf_1141`, `cf_1143`, `cf_1145`, `cf_1147`, `cf_1149`, `cf_1151`, `cf_1153`, `cf_1155`, `cf_1157`, `cf_1159`, `cf_1161`, `cf_1163`, `cf_1165`, `cf_1167`, `cf_1169`, `cf_1171`, `cf_1173`, `cf_1175`, `cf_1177`, `cf_1179`, `cf_1181`, `cf_1183`, `cf_1185`, `cf_1187`, `cf_1189`, `cf_1191`, `cf_1193`, `cf_1195`, `cf_1197`, `cf_1199`, `cf_1201`, `cf_1203`, `cf_1205`, `cf_1207`, `cf_1209`, `cf_1211`, `cf_1213`, `cf_1215`, `cf_1217`, `cf_1219`, `cf_1221`, `cf_1223`, `cf_1225`, `cf_1227`, `cf_1263`, `cf_1265`, `cf_1267`, `cf_1269`, `cf_1271`, `cf_1273`)";

							'$crmid','$contact_no','$id_estado','','','',''
''
							'$crmid','N','','','','$sexo','','A','','','','$localidad','$municipio','','$tipo_doc_madre','$nro_doc_madre','$apellido_madre','$nombre_madre','','$codigo_postal','SI'


						$id_estado = $crmid + 1;
						$contact_no = "CON";
						$apellido_benef = utf8_encode($apellido_benef);
						$nombre_benef = utf8_encode($nombre_benef);
						$sql = "INSERT INTO vtiger_contactdetails(contactid,contact_no,accountid,firstname,lastname,email,mobile) 
														 VALUES ()";
						$result = mysqli_query($link,$sql);	
						//saco el proximo numero de beneficiario
						$sql = "SELECT (MAX(cf_855) + 1) AS cf_855 FROM vtiger_contactscf";
						$result = mysqli_query($link,$sql);	
						$reg = mysqli_fetch_array($result);
						$clave_beneficiario = $reg['cf_855'];
						
						$sql = "SELECT cf_1073  FROM vtiger_cf_1073 WHERE cf_1073 LIKE '%".$cuie."%'";
						$result = mysqli_query($link,$sql);	
						$reg = mysqli_fetch_array($result);
						$cuie = $reg['cf_1073'];
						
						$calle = utf8_encode($calle);
						$apellido_madre = utf8_encode($apellido_madre);
						$nombre_madre = utf8_encode($nombre_madre);
						$sql = "INSERT INTO vtiger_contactscf(contactid,cf_853,cf_855,cf_865,cf_867,cf_871,cf_877,cf_887,cf_949,cf_1077,cf_963,cf_1023,cf_965,cf_1019,cf_1043,cf_1045,cf_1047,cf_1049,cf_1073,cf_1085,cf_1021) 
	VALUES ('$crmid','N','$clave_beneficiario','$tipo_documento','$numero_doc','$sexo','$pais','A','$calle','$puerta','$departamento','$localidad','$municipio','$clase_documento_benef','$tipo_doc_madre','$nro_doc_madre','$apellido_madre','$nombre_madre','$cuie','$codigo_postal','SI')"; 
						$result = mysqli_query($link,$sql);
						
						$sql = "INSERT INTO vtiger_contactsubdetails(contactsubscriptionid, birthday) VALUES ('$crmid','$fecha_nacimiento')";
						$result = mysqli_query($link,$sql);
						$sql = "INSERT INTO vtiger_customerdetails(customerid) VALUES ('$crmid')";
						$result = mysqli_query($link,$sql);
						$sql = "INSERT INTO vtiger_customerdetails(customerid) VALUES ('$crmid')";
						$result = mysqli_query($link,$sql);
						
						$sql = "SELECT * FROM vtiger_modtracker_basic_seq";
						$result = mysqli_query($link,$sql);	
						$reg = mysqli_fetch_array($result);
						$tracker_id = $reg['id'] + 1;
						
						$sql = "INSERT INTO vtiger_modtracker_basic(id, crmid, module, whodid, changedon, status) 
						VALUES ('$tracker_id','$crmid','Contacts','1', CURDATE(),'2')";
						$result = mysqli_query($link,$sql);
						$tracker_id = $tracker_id + 1;
						$sql = "INSERT INTO vtiger_modtracker_basic(id, crmid, module, whodid, changedon, status) 
						VALUES ('$tracker_id','$crmid','Contacts','1', CURDATE(),'4')";
						$result = mysqli_query($link,$sql);
						
						//omitimos grabar en la tabla vtiger_modtracker_detail
						
						//grabo el proximo crmid
						$sql = "UPDATE vtiger_crmentity_seq SET id='$crmid'";
						$result = mysqli_query($link,$sql);
						//grabo el proximo tracker_id
						$sql = "UPDATE vtiger_modtracker_basic_seq SET id='$tracker_id'";
						$result = mysqli_query($link,$sql);
						
						//grabo el estado del beneficiario
						$sql = "SELECT * FROM vtiger_crmentity_seq";
						$result = mysqli_query($link,$sql);	
						$reg = mysqli_fetch_array($result);
						$crmid = $reg['id'] + 1;
						
						$sql = "INSERT INTO vtiger_crmentity(crmid, smcreatorid, smownerid, modifiedby, setype, description, createdtime, modifiedtime, viewedtime, status, version, presence, deleted, smgroupid, source, label) 
														VALUES ('$crmid','1','1','1','Accounts','','$fecha_inscripcion','$fecha_inscripcion','','','0','1','0','0','Alt.Aut.','')";
						$result = mysqli_query($link,$sql);
						
						$sql = "INSERT INTO vtiger_crmentity_user_field(recordid, userid, starred) VALUES ('$crmid','1','0')";
						$result = mysqli_query($link,$sql);
						$sql = "INSERT INTO vtiger_account(accountid, accountname) VALUES ('$crmid','$crmid')";
						$result = mysqli_query($link,$sql);
						$sql = "INSERT INTO vtiger_accountbillads(accountaddressid) VALUES ('$crmid')";
						$result = mysqli_query($link,$sql);
						$sql = "INSERT INTO vtiger_accountscf(accountid, cf_1095, cf_1099, cf_1111) VALUES ('$crmid','$clave_beneficiario','N','N')";
						$result = mysqli_query($link,$sql);
						$sql = "INSERT INTO vtiger_accountshipads(accountaddressid) VALUES ('$crmid')";
						$result = mysqli_query($link,$sql);
						
						$sql = "SELECT * FROM vtiger_modtracker_basic_seq";
						$result = mysqli_query($link,$sql);	
						$reg = mysqli_fetch_array($result);
						$tracker_id = $reg['id'] + 1;
						
						//omitimos grabar en la tabla vtiger_modtracker_detail
						$sql = "INSERT INTO vtiger_modtracker_basic(id, crmid, module, whodid, changedon, status) 
						VALUES ('$tracker_id','$crmid','Accounts','1', CURDATE(),'2')";
						$result = mysqli_query($link,$sql);
						
						$resta_crmid = $crmid - 1;
						$sql = "INSERT INTO vtiger_crmentityrel(crmid, module, relcrmid, relmodule) VALUES ('$resta_crmid','Contacts','$crmid','Accounts')";
						$result = mysqli_query($link,$sql);
						
						
						
						//grabo el proximo crmid
						$sql = "UPDATE vtiger_crmentity_seq SET id='$crmid'";
						$result = mysqli_query($link,$sql);					
						//grabo el proximo tracker_id
						$sql = "UPDATE vtiger_modtracker_basic_seq SET id='$tracker_id'";
						$result = mysqli_query($link,$sql);
						
						$cantidad_consultas++; 

					
					}else{
						$mensaje_control = "error en el registro del archivo origen.<br>";
						$cantidad_consultas_error++; 
					} 
					
				}

				$msj_aviso = "FIN DEL PROCESO. Cantidad de registros <strong>$x</strong> consultas. <br>";
				$msj_aviso .= "Se ejecutaron correctamente <strong>$cantidad_consultas</strong> consultas. <br>";
				$msj_aviso .= "Se ejecutaron con errores <strong>$cantidad_consultas_error</strong> consultas.";
			}
			fclose($errores);
			$var_php = "<script>document.writeln(fin());</script>";
			echo $var_php;
			
		}
	}else{
		echo "Error de subida";
	}
}

?>
<script>
    $(document).ready(function () {
        $("#btnExport").click(function (e) {
            window.open('data:application/vnd.ms-excel,' + encodeURIComponent($('#benef_no_estado').html()));
            e.preventDefault();
        });
    });
	
</script>
<style>
    .ins_box_60p {
        width: 60%;
    }

    .marg_15top {
        margin-top: 15px;
    }
    .centrado_margin {
        margin: 0 auto;
    }
    .centrado_txt {
        text-align:  center;
    }
    .redondeado3 {
        border: 1px solid #cccccc;
        border-radius: 3px;
        -webkit-border-radius: 3px 3px 3px;
    }

    .borde_gris {
        border: 1px solid #b3b3b3;
    }

    .fondo_gris {
        background-color: #f1f1f1;
    }

    .pad5 {
        padding: 5px;
    }

    .marg5 {
        margin: 5px;
    }
</style>
<div class="ins_box_60p centrado_margin marg_15top redondeado3 fondo_gris">
	
		
	<div class="centrado_txt pad5">
		<div id="ajaxBusy" style="display:none;">
			<BR>PROCESANDO ARCHIVO, ESPERE POR FAVOR... <BR><BR>
			<img src="wait.gif"><BR><BR>
		</div>
    </div>

	
    <?php
    if ($mostrar_form) {
        ?>
        <form action="altas_automaticas.php" method="POST" enctype="multipart/form-data">
            <table style="margin: 0 auto; margin-top: 10px;" class="marg5">
                <tr>
                    <td>
                        <input type="file" name="archivo_estado" id="archivo_estado"/>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" class="centrado_txt">
                        <div class="marg5">
                            <input type="submit" name="boton_submit" value="Cargar" onclick="$('#ajaxBusy').show();">
                        </div>
                    </td>
                </tr>
            </table>
            <hr style="background-color: gray; height: 1px; border: 0;"/>
        </form>
    <?php } else { ?>
        <div class="centrado_txt pad5">
            <?php echo $msj_aviso ?>
        </div>
    <?php } ?>
</div>