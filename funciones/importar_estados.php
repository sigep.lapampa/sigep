<link rel="stylesheet" type="text/css" href="https://www.jeasyui.com/easyui/themes/default/easyui.css">
<link rel="stylesheet" type="text/css" href="https://www.jeasyui.com/easyui/themes/icon.css">
<link rel="stylesheet" type="text/css" href="https://www.jeasyui.com/easyui/themes/color.css">
<link rel="stylesheet" type="text/css" href="https://www.jeasyui.com/easyui/demo/demo.css">
<script type="text/javascript" src="https://code.jquery.com/jquery-1.9.1.min.js"></script>
<script type="text/javascript" src="https://www.jeasyui.com/easyui/jquery.easyui.min.js"></script> 
	
<script language="javascript" type="text/javascript">
var resultado;
var lat;
var lon;
function inicio(){
	$('#ajaxBusy').show();
	return '';
}
function fin(){
	$('#ajaxBusy').hide();
	return '';
}

</script>


<?php
include_once 'conectar.php';

//AUMENTAMOS EL TAMAÑO DE LOS ARCHIVOS PHP
ini_set('post_max_size', '30M');
ini_set('upload_max_filesize', '30M');
ini_set('max_execution_time', '1000');
ini_set('max_input_time', '1000');

$mostrar_form = true;
set_time_limit(0);

if (isset($_FILES['archivo_estado'])) { //Tenemos el archivo de estados
    $mostrar_form = false;
    //SI EL ARCHIVO SE ENVIA Y ADEMAS SE SUBIO CORRECTAMENTE
    if (isset($_FILES["archivo_estado"])) {
		
		?>
		<div class="ins_box_60p centrado_margin marg_15top redondeado3 fondo_gris">
			<div class="centrado_txt pad5">
				<div id="ajaxBusy">
					<BR>PROCESANDO ARCHIVO, ESPERE POR FAVOR... <BR><BR>
					<img src="wait.gif"><BR><BR>
				</div>
		    </div>
		</div>	
		<?php
		
		
		//voy a crear un listado con los codigos sis y los cuie 
	$array_cuie = array(P03787 => '1', P03784 => '2', P03788 => '3', P03027 => '5', P03023 => '7', P03795 => '10', P03789 => '11', P06453 => '12', P03798 => '14', P03028 => '15',
						P01159 => '16', P06479 => '17', P03801 => '20', P03800 => '21', P03802 => '22', P03022 => '23', P03805 => '25', P06454 => '26', P03807 => '27', P03809 => '28',
						P03810 => '29', P06465 => '30', P03031 => '32', P03025 => '34', P06467 => '36', P03024 => '37', P03030 => '38', P06483 => '39', P03026 => '40', P03808 => '43',
						P03799 => '44', P03786 => '45', P06447 => '46', P06480 => '48', P01172 => '49', P03785 => '50', P03032 => '51', P03034 => '52', P03029 => '54', P03803 => '55',
						P06460 => '57', P06450 => '60', P06486 => '62', P06488 => '63', P06468 => '65', P06455 => '66', P03806 => '67', P01173 => '68', P01175 => '69', P06471 => '71',
						P03791 => '72', P03811 => '73', P01170 => '74', P06456 => '75', P03812 => '76', P03813 => '77', P03814 => '78', P06459 => '79', P06481 => '80', P01176 => '81',
						P01169 => '82', P06482 => '83', P01174 => '84', P06470 => '88', P06473 => '90', P06462 => '91', P01171 => '92', P06476 => '112', P01155 => '160', P01536 => '161',
						P01156 => '163', P01157 => '164', P03797 => '165', P01529 => '166', P03816 => '64', P06451 => '19', P06449 => '33', P06458 => '1062', P06446 => '61', P06477 => '70',
						P06475 => '41', P06474 => '89', P03804 => '24', P06461 => '47', P03033 => '18', P03796 => '13', P03794 => '8', P03792 => '53', P03790 => '6', P03793 => '9',
						P03815 => '35',P06496 => '85',P06452 => '120',P06494 => '162',P06495 => '167',P06493 => '168',P06497 => '97');

        //SE ABRE EL ARCHIVO EN MODO LECTURA
        $fp = fopen($_FILES['archivo_estado']['tmp_name'], "r");
        //SE RECORRE
        $nombre_tmp = $_FILES['archivo_estado']['tmp_name'];
        if (filesize($nombre_tmp) != 0) {

            $arr_datos = array();
            $sql = "";
            $cantidad_consultas = 0;
 //           $fecha_carga = date("Y-m-d");
            $arr_upd = array();

			//genero nombre de archivo
			$filename = "cargar_".$_FILES['archivo_estado']['name'];

			//creo y abro el archivo
			if (!$handle = fopen($filename, "w")) { //'a'
				echo "No se Puede abrir ($filename)";
				exit;
			}else {
				ftruncate($handle, filesize($filename));
			}
			
		
			$x=0;

			//grabo el estado del beneficiario
			$sql = "SELECT * FROM vtiger_crmentity_seq";
			$result = mysqli_query($link,$sql);	
			$reg = mysqli_fetch_array($result);
			$crmid = $reg['id'];

			$contenido="UPDATE vtiger_beneficiarioscf SET cf_1205='N',cf_1211='N',cf_1209='S/Estado para este periodo'; ";
			//$sql="UPDATE vtiger_beneficiarioscf SET cf_1205='N',cf_1211='N',cf_1209='S/Estado para este periodo'";
			//$result = mysqli_query($link,$sql);

			while ($arr_estado = fgetcsv ($fp, 1000, ";")) {

				$crmid++;
				$clave_beneficiario = preg_replace('/[\x00-\x1F\x80-\xFF]/', '', $arr_estado[0]);
				$activo = $arr_estado[1];
				$MotivoBaja = $arr_estado[2];
		 		$MensajeBaja = $arr_estado[3];
				$CEB = $arr_estado[4];
				$CUIE = $arr_estado[5];
				$FechaUltimaPrestacion = $arr_estado[6];
				$codigo_prestacion = $arr_estado[7];
				$nombre_estado = "Estado";
				$fecha_estado = $_POST['fecha_estado'];
				//$fecha_estado = $arr_estado[9];
				$asignado_a = 1; //administrador
				//$fechaActual = date('Y-m-d');
				$fechaActual = $_POST['fechaActual'];
				$beneficiariosid='';
					
					$sql = "INSERT INTO vtiger_crmentity(crmid, smcreatorid, smownerid, modifiedby, setype, description, createdtime, modifiedtime, viewedtime, status, version, presence, deleted, smgroupid, source, label) VALUES ('$crmid','1','1','1','Estados','','$fechaActual','$fechaActual','','','0','1','0','0','Estados.','')";
					$contenido.=$sql.'; ';
					//$result = mysqli_query($link,$sql);
					
					$sql = "INSERT INTO vtiger_crmentity_user_field(recordid, userid, starred) VALUES ('$crmid','1','0')";
					$contenido.=$sql.'; ';
					//$result = mysqli_query($link,$sql);
					$sql = "INSERT INTO vtiger_estados(estadosid, estadosno) VALUES ('$crmid','$crmid')";
					$contenido.=$sql.'; ';
					//$result = mysqli_query($link,$sql);
					
					$sql = "SELECT beneficiariosid FROM vtiger_beneficiarioscf WHERE cf_1099='$clave_beneficiario'";
					$result = mysqli_query($link,$sql);	
					$reg = mysqli_fetch_array($result);
					$beneficiariosid = $reg['beneficiariosid'];

					$sql = "UPDATE vtiger_beneficiarioscf SET cf_1205='$activo',cf_1211='$CEB',cf_1209='$MensajeBaja' WHERE beneficiariosid='$beneficiariosid'";
					$contenido.=$sql.'; '; 
					//$result = mysqli_query($link,$sql);

					$sql = "INSERT INTO vtiger_estadoscf (`estadosid`, `cf_1237`, `cf_1239`, `cf_1241`, `cf_1243`, `cf_1245`, `cf_1247`, `cf_1249`, `cf_1251`, `cf_1253`, `cf_1255`, `cf_1257`) VALUES ('$crmid','$activo', '$MotivoBaja', '$MensajeBaja', '$CEB', '$CUIE', '$FechaUltimaPrestacion', '$codigo_prestacion','$fecha_estado','$fechaActual','$clave_beneficiario','$beneficiariosid')";
					$contenido.=$sql.'; 
';
					//$result = mysqli_query($link,$sql);

					$cantidad_consultas++; 

					fwrite($handle, $contenido);
					$contenido='';
			}

			//grabo el proximo crmid
			$sql = "UPDATE vtiger_crmentity_seq SET id='$crmid'";
			$result = mysqli_query($link,$sql);	

			$msj_aviso = "FIN DEL PROCESO. Cantidad de registros <strong>$x</strong> consultas. <br>";
			$msj_aviso .= "Se ejecutaron correctamente <strong>$cantidad_consultas</strong> consultas. <br>";
			$msj_aviso .= "Se ejecutaron con errores <strong>$cantidad_consultas_error</strong> consultas.";
		
			fclose($handle);
			$var_php = "<script>document.writeln(fin());</script>";
			echo $var_php;
			
		}
	}else{
		echo "Error de subida";
	}
}
?>
<script>
    $(document).ready(function () {
        $("#btnExport").click(function (e) {
            window.open('data:application/vnd.ms-excel,' + encodeURIComponent($('#benef_no_estado').html()));
            e.preventDefault();
        });
    });
	
</script>
<style>
    .ins_box_60p {
        width: 60%;
    }

    .marg_15top {
        margin-top: 15px;
    }
    .centrado_margin {
        margin: 0 auto;
    }
    .centrado_txt {
        text-align:  center;
    }
    .redondeado3 {
        border: 1px solid #cccccc;
        border-radius: 3px;
        -webkit-border-radius: 3px 3px 3px;
    }

    .borde_gris {
        border: 1px solid #b3b3b3;
    }

    .fondo_gris {
        background-color: #f1f1f1;
    }

    .pad5 {
        padding: 5px;
    }

    .marg5 {
        margin: 5px;
    }
</style>
<div class="ins_box_60p centrado_margin marg_15top redondeado3 fondo_gris">
	
		
	<div class="centrado_txt pad5">
		<div id="ajaxBusy" style="display:none;">
			<BR>PROCESANDO ARCHIVO, ESPERE POR FAVOR... <BR><BR>
			<img src="wait.gif"><BR><BR>
		</div>
    </div>

	
    <?php
    if ($mostrar_form) {
        ?>
        <h3>IMPORTAR ESTADOS</h3>
        <form action="importar_estados.php" method="POST" enctype="multipart/form-data">
            <table style="margin: 0 auto; margin-top: 10px;" class="marg5">
                <tr>
                    <td>
                        <input type="file" name="archivo_estado" id="archivo_estado"/>
                    </td>
                </tr>
                <tr>
                    <td>
                        <input type="date" id="fecha_estado" name="fecha_estado"> : fecha del estado
                    </td>
                </tr>
                <tr>
                    <td>
                        <input type="date" id="fechaActual" name="fechaActual"> : fecha de carga
                    </td>
                </tr>
                <tr>
                    <td colspan="2" class="centrado_txt">
                        <div class="marg5">
                            <input type="submit" name="boton_submit" value="Cargar" onclick="$('#ajaxBusy').show();">
                        </div>
                    </td>
                </tr>
            </table>
            <hr style="background-color: gray; height: 1px; border: 0;"/>
        </form>
    <?php } else { ?>
        <div class="centrado_txt pad5">
            <?php echo $msj_aviso ?>
        </div>
    <?php } ?>
</div>