<?php
/*+**********************************************************************************
 * The contents of this file are subject to the vtiger CRM Public License Version 1.1
 * ("License"); You may not use this file except in compliance with the License
 * The Original Code is:  vtiger CRM Open Source
 * The Initial Developer of the Original Code is vtiger.
 * Portions created by vtiger are Copyright (C) vtiger.
 * All Rights Reserved.
 ************************************************************************************/

//Overrides GetRelatedList : used to get related query
//TODO : Eliminate below hacking solution
include_once 'config.php';
include_once 'include/Webservices/Relation.php';

include_once 'vtlib/Vtiger/Module.php';
include_once 'includes/main/WebUI.php';

/*
//exportar un modulo
require_once('vtlib/Vtiger/Package.php');
//require_once('vtlib/Vtiger/Module.php');
$package = new Vtiger_Package();
$package->export(
      Vtiger_Module::getInstance('Beneficiarios'),
      'vtlib',
      'Beneficiarios-Export.zip',
      true
); */

//  $adb->setDebug(true);
/*
//desinstalar un modulo
include_once 'vtlib/Vtiger/Module.php';
$Vtiger_Utils_Log = true;
$module = Vtiger_Module::getInstance('<ModuleName>');
if ($module) $module->delete();

//luego eliminar archivos
//modules/ModuleName
//languages/en_us/ModuleName.php
//languages/.../ModuleName.php
//layouts/v7/modules/ModuleName
//cron/ModuleName

//luego eliminar datos
*/

$webUI = new Vtiger_WebUI();
$webUI->process(new Vtiger_Request($_REQUEST, $_REQUEST));
