/*+***********************************************************************************
 * The contents of this file are subject to the vtiger CRM Public License Version 1.0
 * ("License"); You may not use this file except in compliance with the License
 * The Original Code is: vtiger CRM Open Source
 * The Initial Developer of the Original Code is vtiger.
 * Portions created by vtiger are Copyright (C) vtiger.
 * All Rights Reserved.
 *************************************************************************************/

//controlo si es una pagina que esta editando un beneficiario o cargando uno nuevo
function editando_contacto() {

	var query = window.location.search.substring(1);
	var vars = query.split("&");
	var pass1, pass2, pass3 = false;
	for (var i=0; i < vars.length; i++) {
	   var pair = vars[i].split("=");
	   if(pair[0] == 'record') {
		   pass1 = true;
	   }
	   if(pair[1] == 'Beneficiarios') {
		   pass2 = true;
	   }
	   if(pair[1] == 'Edit') {
		   pass3 = true;
	   } 
	}

	if((pass1) && (pass2) && (pass3)){
		return true;
	}else{
		return false;
	}

}

//validar el beneficiario cargado antes de guardar
function validar() {

	var doc = $('[name=cf_1079]').val();
	var tipo_doc = $('[name=cf_1077]').val();
	var e = false;
	
	//si el documento es ajeno no tengo que controlar duplicados
	if($('[name=cf_1075]').val() == 'PROPIO'){
	
		$.ajax({
			url: "funciones/consultas.php?validar_beneficiario=1&tipo_doc="+tipo_doc+"&doc="+doc,
			async: false,
			type: "get",
			data: doc,
			beforeSend: function(){
				//$("#load").show();
			},
			dataType: 'html',
			success: function(data){

				if(data){
					
					$("#dialog2").dialog({
						closeOnEscape: false,
						minWidth: 600,
						modal: true,
						show: "shake",
						open: function(event, ui) { $(this).parent().find(".ui-dialog-titlebar-close").remove(); },
						buttons: {
							'Ingresar datos nuevamente': function () {
								$(this).dialog("close");
								document.getElementById("Contacts_editView_fieldName_cf_1079").focus();
							},
							'Cancelar Inscripcion': function () {
								 window.location.href = "index.php?module=Beneficiarios&view=List&viewname=57&app=MARKETING";
							}
						}
					});
					$("#dialog2").html(data);
				}else{
					document.edit.submit();
				}

			}
		}); 
	

/*	$.confirm({
	    title: 'Beneficiarios',
	    content: '<h4>El Beneficiario se guardo correctamente.</h4>',
	    type: 'red',
	    columnClass: 'col-md-4 col-md-offset-4',
	    typeAnimated: true,
	    buttons: {
	        Ok: function () {
	        }
	    }
	}); */

	}else{
		document.edit.submit();
	}

}

$(document).ready(function() {
	
	//no deja que se ejecute el submit
	//lo utilizo para controlar que no dupliquen datos
	$("#EditView").on('submit', function(evt){
		if(!editando_contacto()){
			evt.preventDefault();  
			$(':input[type="submit"]').prop('disabled', false);
			evt.stopPropagation()
			validar();
		}
	}); 

	//controlo la fecha probable de parto
	$('[name=cf_1153]').blur(function(){
		controlar_fpp($('[name=cf_1149]').val(),$('[name=cf_1151]').val());
	});

	/**
	 * Calcula y controla la fecha probable de parto
	 * @param input_diagnotisco = fecha de diagnotisco , input_semanas = semanas de embarazo
	*/
	function controlar_fpp(input_diagnotisco, input_semanas) {

	    var fecha_diagnotisco = input_diagnotisco.replace('-','/');
	    fecha_diagnotisco = fecha_diagnotisco.replace('-','/');
	    var semanas = input_semanas;
	    if ((fecha_diagnotisco != "") && (semanas != "")) {
	        var dias_fecha_inicial = ((38 - semanas) * 7);
	        var dias_fecha_final = ((42 - semanas) * 7);
	        var fpp_inicial = sumaFecha(dias_fecha_inicial, fecha_diagnotisco);
	        var fpp_final = sumaFecha(dias_fecha_final, fecha_diagnotisco);
	        var fpp_ingresada = $('[name=cf_1153]').val();
	        if (($.datepicker.parseDate('dd/mm/yy', fpp_ingresada) < $.datepicker.parseDate('dd/mm/yy', fpp_inicial)) || ($.datepicker.parseDate('dd/mm/yy', fpp_ingresada) > $.datepicker.parseDate('dd/mm/yy', fpp_final))) {
	            $("#dialog2").html("<p style='text-align:center'><span class='ui-icon ui-icon-alert' style='float:left; margin:0 7px 20px 0;'></span>La fecha probable de parto, no pertenece a un rango correcto, esta debería de estar entre " + fpp_inicial + " y " + fpp_final + "</p>");
	            $("#dialog2").dialog({
	                closeOnEscape: false,
	                minWidth: 500,
	                modal: true,
	                show: "shake",
					open: function(event, ui) { $(this).parent().find(".ui-dialog-titlebar-close").remove(); },
	                buttons: {
	                    "Fecha sugerida": function () {
	                        $(this).dialog("close");
	                        fpp_final=fpp_final.replace('/','-');
	                        fpp_final=fpp_final.replace('/','-');
	                        $('[name=cf_1153]').val(fpp_final);
	                    },
	                    "Descartar embarazo": function () {
	                        $(this).dialog("close");
	                        $('[name=cf_1145]').val('').trigger('change');
							$('[name=cf_1147]').val('');
							$('[name=cf_1149]').val('');
							$('[name=cf_1151]').val('');
							$('[name=cf_1153]').val('');
							$('[name=cf_1155]').val('');
	                    },
	                    Entendido: function () {
	                        $(this).dialog("close");
	                    }
	                }
	            });
	        }
	    }
	}

	/**
	 * Suma "d" dias a una fecha determinada
	 * @param d = dias, fecha = fecha determinada
	 * @return fecha resultado
	 */
	function sumaFecha(d, fecha) {

	    var Fecha = new Date();
	    var sFecha = fecha || (Fecha.getDate() + "/" + (Fecha.getMonth() + 1) + "/" + Fecha.getFullYear());
	    var sep = sFecha.indexOf('/') != -1 ? '/' : '-';
	    var aFecha = sFecha.split(sep);
	    var fecha = aFecha[2] + '/' + aFecha[1] + '/' + aFecha[0];
	    fecha = new Date(fecha);
	    fecha.setDate(fecha.getDate() + parseInt(d));
	    var anno = fecha.getFullYear();
	    var mes = fecha.getMonth() + 1;
	    var dia = fecha.getDate();
	    mes = (mes < 10) ? ("0" + mes) : mes;
	    dia = (dia < 10) ? ("0" + dia) : dia;
	    var fechaFinal = dia + sep + mes + sep + anno;
	    return (fechaFinal);

	}

	//Select sexo F/M
	$('[name=cf_1273]').change(function(){ 
		
		if($('[name=cf_1273]').val() == "M"){ 

			$('[name=cf_1145]').val('').trigger('change');
			$('[name=cf_1145]').prop('disabled', true);
			$('[name=cf_1147]').val('');
			$('[name=cf_1147]').prop('disabled', true);
			$('[name=cf_1149]').val('');
			$('[name=cf_1149]').prop('disabled', true);
			$('[name=cf_1151]').val('');
			$('[name=cf_1151]').prop('disabled', true);
			$('[name=cf_1153]').val('');
			$('[name=cf_1153]').prop('disabled', true);
			$('[name=cf_1155]').val('');
			$('[name=cf_1155]').prop('disabled', true);

		}else{

			$('[name=cf_1145]').prop('disabled', false);
			$('[name=cf_1147]').prop('disabled', false);
			$('[name=cf_1149]').prop('disabled', false);
			$('[name=cf_1151]').prop('disabled', false);
			$('[name=cf_1153]').prop('disabled', false);
			$('[name=cf_1155]').prop('disabled', false);

		}


	});

	//select embarazada SI/NO
	$('[name=cf_1145]').change(function(){
		if($('[name=cf_1145]').val() == "SI"){
			document.querySelector('[name=cf_1147]').required = true;//fum
			document.querySelector('[name=cf_1149]').required = true;//fecha diagnostico embarazo
			document.querySelector('[name=cf_1151]').required = true;//semanas de embarazo
			document.querySelector('[name=cf_1153]').required = true;//fecha probable de parto
		}else{
			document.querySelector('[name=cf_1147]').required = false;//fum
			document.querySelector('[name=cf_1149]').required = false;//fecha diagnostico embarazo
			document.querySelector('[name=cf_1151]').required = false;//semanas de embarazo
			document.querySelector('[name=cf_1153]').required = false;//fecha probable de parto
		}
	});

	//solo lectura para los select municipio, codigo postal y departamento
	$("[name=cf_1131]").prop('readonly', true);
	$("[name=cf_1263]").prop('readonly', true);
	$("[name=cf_1135]").prop('readonly', true);

	//desea cambiar el efector habital?
	$("#efector").dialog({
		closeOnEscape: false,
		minWidth: 500,
		modal: true,
		show: "shake",
		open: function(event, ui) { $(this).parent().find(".ui-dialog-titlebar-close").remove(); },
		buttons: {
			'Aceptar': function () {
				$('[name=cf_1201]').val(document.getElementById("efe_hab").value).trigger('change');
				$(this).dialog("close");
			},
			'Cancelar': function () {
				$(this).dialog("close");
			}
		}
	});

	$("#Beneficiarios_editView_fieldName_cf_1079").blur(function(){
		if($("#Beneficiarios_editView_fieldName_cf_1079").val() != ''){
			consultar_renaper();
		}
	});

	function consultar_renaper() {

		var v = document.getElementById("Beneficiarios_editView_fieldName_cf_1079").value;
		var clase_doc = $('[name=cf_1075]').val();
		var e = false;

		var messageBar = jQuery('#messageBar');
        	messageBar.removeClass('hide');

		$.ajax({
			url: "funciones/consultas.php?clase_doc="+clase_doc+"&v="+v,
			async: false,
			type: "post",
			data: v,
			beforeSend: function(){
				messageBar.html('<div style="text-align:center;position:fixed;top:50%;left:40%;"><img src="'+app.vimage_path('loading.gif')+'"></div>');				
			},
			dataType: 'html',
			success: function(data){
				
				if(data){
					
					$("#dialog2").dialog({
						closeOnEscape: false,
						minWidth: 600,
						modal: true,
						show: "shake",
						open: function(event, ui) { $(this).parent().find(".ui-dialog-titlebar-close").remove(); },
						buttons: {
							'Nuevo Beneficiario': function () {
								$(this).dialog("close");
								window.location.href = "index.php?module=Beneficiarios&view=Edit&app=MARKETING";
								 e = true;
							},
							'Continuar': function () {
								$(this).dialog("close");
								e = false;
							},
							'Cancelar Inscripcion': function () {
								$(this).dialog("close");
								 window.location.href = "index.php?module=Beneficiarios&view=List&viewname=57&app=MARKETING";
								 e = true;
							}
						}
					});
					
					$("#dialog2").html(data);
				}else{
					e = false;
				}
				messageBar.addClass('hide');
			}
		});  
	  
		if(!e){

			if($('[name=cf_1075]').val() != 'AJENO'){
				$.ajax({
					url: "funciones/sisa.php?v="+v,
					type: "post",
					data: v,
					dataType: 'json',
					complete:function(){
						messageBar.html('<div style="text-align:center;position:fixed;top:50%;left:40%;"><img src="'+app.vimage_path('loading.gif')+'"></div>');
					},
					success: function(data){
					/*	$('#Contacts_editView_fieldName_lastname').val(data.apellido[0]);
						$('#Contacts_editView_fieldName_firstname').val(data.nombre[0]);
						
						$('#Contacts_editView_fieldName_birthday').val(data.fechaNacimiento[0].substr(0, data.fechaNacimiento[0].length - 6));
						$('#Contacts_editView_fieldName_cf_949').val(data.domicilio[0]);
						$('#Contacts_editView_fieldName_cf_1085').val(data.codigo_postal[0]);
						$('#Contacts_editView_fieldName_cf_963').val(data.departamento[0]);
						$('#Contacts_editView_fieldName_cf_965').val(data.localidad[0]);
						$('#account_id_display').val('aaaa');
						
						//$('[name=FIELDNAME]').val('NEW VALUE').trigger('change');
						$('[name=cf_871]').val(data.sexo[0]).trigger('change');
						$('[name=cf_873]').val(data.provincia[0]).trigger('change');
						$('[name=cf_877]').val(data.paisNacimiento[0]).trigger('change');
						$('[name=cf_865]').val(data.tipoDocumento[0]).trigger('change');
						$('[name=cf_1023]').val(data.localidad[0]).trigger('change');
						
						var birthday_arr = data.fechaNacimiento[0].substr(0, data.fechaNacimiento[0].length - 6).split("-");
						var birthday_date = new Date(birthday_arr[2], birthday_arr[1] - 1, birthday_arr[0]);
						var ageDifMs = Date.now() - birthday_date.getTime();
						var ageDate = new Date(ageDifMs);
						
						if(Math.abs(ageDate.getUTCFullYear() - 1970) < 14){
							document.querySelector('[name=cf_1041]').required = true;
							document.querySelector('[name=cf_1043]').required = true;
							document.querySelector('[name=cf_1045]').required = true;
							document.querySelector('[name=cf_1047]').required = true;
							document.querySelector('[name=cf_1049]').required = true;
						}else{
							document.querySelector('[name=cf_1041]').required = false;
							document.querySelector('[name=cf_1043]').required = false;
							document.querySelector('[name=cf_1045]').required = false;
							document.querySelector('[name=cf_1047]').required = false;
							document.querySelector('[name=cf_1049]').required = false;
						}*/

						messageBar.addClass('hide');
						
					}
				});
			}else{
				//$("#load").hide();
			}

		}else{
			//limpiar_form();
			//$("#load").hide();
		}
		
	}
	

});
