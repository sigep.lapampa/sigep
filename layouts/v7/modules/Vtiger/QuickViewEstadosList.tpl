{*<!--
/*********************************************************************************
** The contents of this file are subject to the vtiger CRM Public License Version 1.0
* ("License"); You may not use this file except in compliance with the License
* The Original Code is: vtiger CRM Open Source
* The Initial Developer of the Original Code is vtiger.
* Portions created by vtiger are Copyright (C) vtiger.
* All Rights Reserved.
*
********************************************************************************/
-->*}
{assign var="COMMENT_TEXTAREA_DEFAULT_ROWS" value="2"}
<div class = "summaryWidgetContainer">
    <div class="recentComments">
        <div class="commentsBody container-fluid" style = "height:100%">
            
                <div class="recentCommentsBody row">
                    <br>
					<div class="commentDetails">
                        <div class="singleComment">
							<div class="container-fluid">
                                    <div class="update_info">
									
										{$HISTORIAL_ESTADOS}
									
									</div>
							</div>		
						</div>
					</div>
                </div>
            
        </div>
    </div>
</div>