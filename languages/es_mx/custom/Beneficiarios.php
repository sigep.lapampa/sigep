<?php
$languageStrings = array(
'CHILE'	=>	'CHILE',
'BRASIL'	=>	'BRASIL',
'PARAGUAY'	=>	'PARAGUAY',
'BOLIVIA'	=>	'BOLIVIA',
'MENDOZA'	=>	'MENDOZA',
'RIO NEGRO'	=>	'RIO NEGRO',
'NEUQUEN'	=>	'NEUQUEN',
'SAN LUIS'	=>	'SAN LUIS',
'25 DE MAYO'	=>	'25 DE MAYO',
'ABRAMO'	=>	'ABRAMO',
'ADOLFO VAN PRAET'	=>	'ADOLFO VAN PRAET',
'AGUSTONI'	=>	'AGUSTONI',
'ALGARROBO DEL AGUILA'	=>	'ALGARROBO DEL AGUILA',
'ALPACHIRI'	=>	'ALPACHIRI',
'ALTA ITALIA'	=>	'ALTA ITALIA',
'ANGUIL'	=>	'ANGUIL',
'ARATA'	=>	'ARATA',
'ATALIVA ROCA'	=>	'ATALIVA ROCA',
'BERNARDO LARROUDE'	=>	'BERNARDO LARROUDE',
'BERNASCONI'	=>	'BERNASCONI',
'CACHIRULO'	=>	'CACHIRULO',
'CALEUFU'	=>	'CALEUFU',
'CARRO QUEMADO'	=>	'CARRO QUEMADO',
'CATRILO'	=>	'CATRILO',
'CEBALLOS'	=>	'CEBALLOS',
'CHACHARRAMENDI'	=>	'CHACHARRAMENDI',
'CHAMAICO'	=>	'CHAMAICO',
'COLONIA BARON'	=>	'COLONIA BARON',
'COLONIA CHICA'	=>	'COLONIA CHICA',
'COLONIA SANTA MARIA'	=>	'COLONIA SANTA MARIA',
'COLONIA SANTA TERESA'	=>	'COLONIA SANTA TERESA',
'CONHELO'	=>	'CONHELO',
'CUCHILLO CO'	=>	'CUCHILLO CO',
'DOBLAS'	=>	'DOBLAS',
'DORILA'	=>	'DORILA',
'EDUARDO CASTEX'	=>	'EDUARDO CASTEX',
'EMBAJADOR MARTINI'	=>	'EMBAJADOR MARTINI',
'EMILIO MITRE'	=>	'EMILIO MITRE',
'FALUCHO'	=>	'FALUCHO',
'GENERAL ACHA'	=>	'GENERAL ACHA',
'GENERAL MANUEL CAMPOS'	=>	'GENERAL MANUEL CAMPOS',
'GENERAL PICO'	=>	'GENERAL PICO',
'GENERAL SAN MARTIN'	=>	'GENERAL SAN MARTIN',
'GOBERNADOR DUVAL'	=>	'GOBERNADOR DUVAL',
'GUATRACHE'	=>	'GUATRACHE',
'HILARIO LAGOS'	=>	'HILARIO LAGOS',
'INGENIERO FOSTER'	=>	'INGENIERO FOSTER',
'INGENIERO LUIGGI'	=>	'INGENIERO LUIGGI',
'INTENDENTE ALVEAR'	=>	'INTENDENTE ALVEAR',
'JACINTO ARAUZ'	=>	'JACINTO ARAUZ',
'LA ADELA'	=>	'LA ADELA',
'LA GLORIA'	=>	'LA GLORIA',
'LA HUMADA'	=>	'LA HUMADA',
'LA MARUJA'	=>	'LA MARUJA',
'LA REFORMA'	=>	'LA REFORMA',
'LIMAY MAHUIDA'	=>	'LIMAY MAHUIDA',
'LONQUIMAY'	=>	'LONQUIMAY',
'LOVENTUE'	=>	'LOVENTUE',
'LUAN TORO'	=>	'LUAN TORO',
'MACACHIN'	=>	'MACACHIN',
'MAISONNAVE'	=>	'MAISONNAVE',
'MAURICIO MAYER'	=>	'MAURICIO MAYER',
'METILEO'	=>	'METILEO',
'MIGUEL CANE'	=>	'MIGUEL CANE',
'MIGUEL RIGLOS'	=>	'MIGUEL RIGLOS',
'MONTE NIEVAS'	=>	'MONTE NIEVAS',
'PARERA'	=>	'PARERA',
'PERU'	=>	'PERU',
'PICHI HUINCA'	=>	'PICHI HUINCA',
'PUELCHES'	=>	'PUELCHES',
'PUELEN'	=>	'PUELEN',
'QUEHUE'	=>	'QUEHUE',
'QUEMU QUEMU'	=>	'QUEMU QUEMU',
'QUETREQUEN'	=>	'QUETREQUEN',
'RANCUL'	=>	'RANCUL',
'REALICO'	=>	'REALICO',
'RELMO'	=>	'RELMO',
'ROLON'	=>	'ROLON',
'RUCANELO'	=>	'RUCANELO',
'SANTA ISABEL'	=>	'SANTA ISABEL',
'SARAH'	=>	'SARAH',
'SPELUZZI'	=>	'SPELUZZI',
'TELEN'	=>	'TELEN',
'TOMAS ANCHORENA'	=>	'TOMAS ANCHORENA',
'TREBOLARES'	=>	'TREBOLARES',
'TRENEL'	=>	'TRENEL',
'UNANUE'	=>	'UNANUE',
'URIBURU'	=>	'URIBURU',
'UTRACAN'	=>	'UTRACAN',
'VERTIZ'	=>	'VERTIZ',
'VICTORICA'	=>	'VICTORICA',
'VILLA MIRASOL'	=>	'VILLA MIRASOL',
'WINIFREDA'	=>	'WINIFREDA',
'ATREUCO'	=>	'ATREUCO',
'CALEU CALEU'	=>	'CALEU CALEU',
'CHALILEO'	=>	'CHALILEO',
'CHAPALEUFU'	=>	'CHAPALEUFU',
'CHICAL CO'	=>	'CHICAL CO',
'CURACO'	=>	'CURACO',
'HUCAL'	=>	'HUCAL',
'LIHUEL CALEL'	=>	'LIHUEL CALEL',
'MARACO'	=>	'MARACO',
'COLONIA 25 DE MAYO'	=>	'COLONIA 25 DE MAYO',
'COR. HILARIO LAGOS'	=>	'COR. HILARIO LAGOS',
'MAISONAVE'	=>	'MAISONAVE',
'AJENO'	=>	'AJENO',
'TOAY'	=>	'TOAY',
'6305'	=>	'6305',
'6301'	=>	'6301',
'6307'	=>	'6307',
'8138'	=>	'8138',
'6326'	=>	'6326',
'6330'	=>	'6330',
'6348'	=>	'6348',
'6354'	=>	'6354',
'6323'	=>	'6323',
'6228'	=>	'6228',
'6221'	=>	'6221',
'6220'	=>	'6220',
'6381'	=>	'6381',
'6383'	=>	'6383',
'6380'	=>	'6380',
'6313'	=>	'6313',
'8336'	=>	'8336',
'6311'	=>	'6311',
'6309'	=>	'6309',
'8212'	=>	'8212',
'8204'	=>	'8204',
'8206'	=>	'8206',
'8208'	=>	'8208',
'6317'	=>	'6317',
'6321'	=>	'6321',
'6319'	=>	'6319',
'6365'	=>	'6365',
'6360'	=>	'6360',
'6361'	=>	'6361',
'8307'	=>	'8307',
'6315'	=>	'6315',
'6331'	=>	'6331',
'6352'	=>	'6352',
'6214'	=>	'6214',
'6213'	=>	'6213',
'6387'	=>	'6387',
'6207'	=>	'6207',
'6212'	=>	'6212',
'6200'	=>	'6200',
'6203'	=>	'6203',
'6205'	=>	'6205',
'6303'	=>	'6303',
'6385'	=>	'6385',
'6367'	=>	'6367',
'6369'	=>	'6369',
'8203'	=>	'8203',
'8214'	=>	'8214',
'6325'	=>	'6325',
'8200'	=>	'8200',
'8201'	=>	'8201',
'COL. SANTA MARIA'	=>	'COL. SANTA MARIA',
'ARAUCANOS'	=>	'ARAUCANOS',
'MAPUCHE'	=>	'MAPUCHE',
'RANQUELES'	=>	'RANQUELES',
'QUECHUA'	=>	'QUECHUA',
'AYMARA'	=>	'AYMARA',
'TEHUELCHE'	=>	'TEHUELCHE',
'PEHUENCHE'	=>	'PEHUENCHE',
'DESCONOCIDA'	=>	'DESCONOCIDA',
'MAPUCHES'	=>	'MAPUCHES',
'AYAMARA'	=>	'AYAMARA',
'P03806 - S. MONTE NIEVAS'	=>	'P03806 - S. MONTE NIEVAS',
'P03792 - C.S. DR. GOMEZ FENTANES - Hilario Lagos'	=>	'P03792 - C.S. DR. GOMEZ FENTANES - Hilario Lagos',
'P01536 - C.S. DEL ESTE - General Pico'	=>	'P01536 - C.S. DEL ESTE - General Pico',
'P03803 - PADRE CASTELLARO - Metileo'	=>	'P03803 - PADRE CASTELLARO - Metileo',
'P03802 - C.S. DR. FRANCISCO LIÑON'	=>	'P03802 - C.S. DR. FRANCISCO LIÑON',
'P03801 - HOSP. DR. PABLO LECUMBERRY - Lonquimay'	=>	'P03801 - HOSP. DR. PABLO LECUMBERRY - Lonquimay',
'P03800 - HOSP. M. Y V. GHIOLDI - La Maruja'	=>	'P03800 - HOSP. M. Y V. GHIOLDI - La Maruja',
'P03799 - C.S. LA HUMADA'	=>	'P03799 - C.S. LA HUMADA',
'P03798 - HOSP. DR. MANUEL FREIRE - Guatrache'	=>	'P03798 - HOSP. DR. MANUEL FREIRE - Guatrache',
'P03797 - C.S. ENERGIA Y PROGRESO'	=>	'P03797 - C.S. ENERGIA Y PROGRESO',
'P03796 - DR. JUAN FACCA - General Campos'	=>	'P03796 - DR. JUAN FACCA - General Campos',
'P03795 - HOSP. DR. PEDRO NOVICK - Doblas'	=>	'P03795 - HOSP. DR. PEDRO NOVICK - Doblas',
'P06454 - DR. LUIS COLOSIO - M. Mayer'	=>	'P06454 - DR. LUIS COLOSIO - M. Mayer',
'P03031 - HOSP. JOSE PADROS - Rancul'	=>	'P03031 - HOSP. JOSE PADROS - Rancul',
'P01159 - HOSP. GOB. CENTENO - General Pico'	=>	'P01159 - HOSP. GOB. CENTENO - General Pico',
'P03790 - HOSP. LUIS A. PETRELLI - Caleufú'	=>	'P03790 - HOSP. LUIS A. PETRELLI - Caleufú',
'P03789 - C.S. DR. LUIS MAZA - B. Larroude'	=>	'P03789 - C.S. DR. LUIS MAZA - B. Larroude',
'P03788 - C.S. RAMON CARRILLO - Ataliva Roca'	=>	'P03788 - C.S. RAMON CARRILLO - Ataliva Roca',
'P03787 - HOSP. ARISTIDES GRANDA - Arata'	=>	'P03787 - HOSP. ARISTIDES GRANDA - Arata',
'P03786 - C.S. JORGE CURCI - Anguil'	=>	'P03786 - C.S. JORGE CURCI - Anguil',
'P03785 - DR. NESTOR SCHANTON - Alta Italia'	=>	'P03785 - DR. NESTOR SCHANTON - Alta Italia',
'P03784 - HOSP. DR. ENRIQUE FERRETI - Alpachiri'	=>	'P03784 - HOSP. DR. ENRIQUE FERRETI - Alpachiri',
'P03816 - C.S. VERTIZ'	=>	'P03816 - C.S. VERTIZ',
'P03805 - HOSP. DR. ANTONIO OLAIZ - M. Riglos'	=>	'P03805 - HOSP. DR. ANTONIO OLAIZ - M. Riglos',
'P06468 - C.S. LA ADELA'	=>	'P06468 - C.S. LA ADELA',
'P03794 - FRANCISCO DEL CALLEJO - Conhelo'	=>	'P03794 - FRANCISCO DEL CALLEJO - Conhelo',
'P03032 - SAMUEL HALFON - Embajador Martini'	=>	'P03032 - SAMUEL HALFON - Embajador Martini',
'P00003 - Mamografo Movil'	=>	'P00003 - Mamografo Movil',
'P06465 - HOSP. VIRGILIO T. URIBURU - Realicó'	=>	'P06465 - HOSP. VIRGILIO T. URIBURU - Realicó',
'P01176 - C.S. VILLA SANTILLAN - Santa Rosa'	=>	'P01176 - C.S. VILLA SANTILLAN - Santa Rosa',
'P03030 - HOSP. ANGEL CIVALERO - Villa Mirasol'	=>	'P03030 - HOSP. ANGEL CIVALERO - Villa Mirasol',
'P03029 - HOSP. DR. JUAN MUNUCE - Jacinto Arauz'	=>	'P03029 - HOSP. DR. JUAN MUNUCE - Jacinto Arauz',
'P03028 - HOSP. DR. LUIS AGOTE - Gral. San Martín'	=>	'P03028 - HOSP. DR. LUIS AGOTE - Gral. San Martín',
'P03027 - HOSP. DR. ROGELIO AMICARELLI - Bernasconi'	=>	'P03027 - HOSP. DR. ROGELIO AMICARELLI - Bernasconi',
'P03026 - HOSP. JUAN B. SMITH - Winifreda'	=>	'P03026 - HOSP. JUAN B. SMITH - Winifreda',
'P03025 - HOSP. DR. MANUEL PEREZ - Santa Isabel'	=>	'P03025 - HOSP. DR. MANUEL PEREZ - Santa Isabel',
'P03024 - HOSP. DR. JULIO TAPIA - Uriburu'	=>	'P03024 - HOSP. DR. JULIO TAPIA - Uriburu',
'P01156 - C.S. GUILLERMO BROWN - General Pico'	=>	'P01156 - C.S. GUILLERMO BROWN - General Pico',
'P03022 - HOSP. DR. HERACLIO LUNA - Macachín'	=>	'P03022 - HOSP. DR. HERACLIO LUNA - Macachín',
'P01157 - C.S. MAURICIO KNOBEL - General Pico'	=>	'P01157 - C.S. MAURICIO KNOBEL - General Pico',
'P01529 - C.S. JOSE RUCCI'	=>	'P01529 - C.S. JOSE RUCCI',
'P03033 - HOSP. DR. JUSTO FERRARI - Ing. Luiggi'	=>	'P03033 - HOSP. DR. JUSTO FERRARI - Ing. Luiggi',
'P01174 - C.S. VILLA GERMINAL Santa Rosa'	=>	'P01174 - C.S. VILLA GERMINAL Santa Rosa',
'P01173 - C.S. BARRIO RIO ATUEL - Santa Rosa'	=>	'P01173 - C.S. BARRIO RIO ATUEL - Santa Rosa',
'P01172 - C.S. EVITA - Santa Rosa'	=>	'P01172 - C.S. EVITA - Santa Rosa',
'P01171 - C.S. NELIDA MALDONADO - FONAVI 42 - Santa...'	=>	'P01171 - C.S. NELIDA MALDONADO - FONAVI 42 - Santa...',
'P01170 - CENTRO SANITARIO - Santa Rosa'	=>	'P01170 - CENTRO SANITARIO - Santa Rosa',
'P01169 - C.S. BARRIO AEROPUERTO - Santa Rosa'	=>	'P01169 - C.S. BARRIO AEROPUERTO - Santa Rosa',
'P06453 - HOSP. DR. PABLO LACOSTE - E. Castex'	=>	'P06453 - HOSP. DR. PABLO LACOSTE - E. Castex',
'P03023 - HOSP. AMADA GATICA - Catriló'	=>	'P03023 - HOSP. AMADA GATICA - Catriló',
'P06462 - C.S. FALUCHO'	=>	'P06462 - C.S. FALUCHO',
'P06450 - C.S. DR. M. GERMAN - Ceballos'	=>	'P06450 - C.S. DR. M. GERMAN - Ceballos',
'P06448 - C.S. LA GLORIA'	=>	'P06448 - C.S. LA GLORIA',
'P06458 - C.S. DOMINGO CABRE - Speluzzi'	=>	'P06458 - C.S. DOMINGO CABRE - Speluzzi',
'P03808 - C.S. PUELCHES'	=>	'P03808 - C.S. PUELCHES',
'P06470 - C.S. PERU'	=>	'P06470 - C.S. PERU',
'P06482 - C.S. ALGARROBO DEL AGUILA'	=>	'P06482 - C.S. ALGARROBO DEL AGUILA',
'P06481 - C.S. UNANUE'	=>	'P06481 - C.S. UNANUE',
'P06480 - C.S. QUEHUE'	=>	'P06480 - C.S. QUEHUE',
'P06476 - C.S. PUELEN'	=>	'P06476 - C.S. PUELEN',
'P06474 - C.S. LIMAY MAHUIDA'	=>	'P06474 - C.S. LIMAY MAHUIDA',
'P06455 - C.S. AGUSTONI'	=>	'P06455 - C.S. AGUSTONI',
'P06463 - C.S. MAISSONAVE'	=>	'P06463 - C.S. MAISSONAVE',
'P06491 - C.S. TREBOLARES'	=>	'P06491 - C.S. TREBOLARES',
'P06461 - C.S. A. VAN PRAET'	=>	'P06461 - C.S. A. VAN PRAET',
'P06460 - C.S. QUETREQUEN'	=>	'P06460 - C.S. QUETREQUEN',
'P03791 - C.S. CHACHARRAMENDI'	=>	'P03791 - C.S. CHACHARRAMENDI',
'P06490 - C.S. SARAH'	=>	'P06490 - C.S. SARAH',
'P06489 - C.S. RUCANELO'	=>	'P06489 - C.S. RUCANELO',
'P06488 - C.S. LOVENTUE'	=>	'P06488 - C.S. LOVENTUE',
'P06487 - C.S. CHAMAICO'	=>	'P06487 - C.S. CHAMAICO',
'P06486 - C.S. CARRO QUEMADO'	=>	'P06486 - C.S. CARRO QUEMADO',
'P06492 - Coordinacion de Maternidad e Infancia'	=>	'P06492 - Coordinacion de Maternidad e Infancia',
'P99999 - Otros'	=>	'P99999 - Otros',
'P06469 - C.S. GOBERNADOR DUVAL'	=>	'P06469 - C.S. GOBERNADOR DUVAL',
'P01155 - C.S. FRANK ALLAN - General Pico'	=>	'P01155 - C.S. FRANK ALLAN - General Pico',
'P06451 - HOSP. DR. REUMANN ENZ - Int. Alvear'	=>	'P06451 - HOSP. DR. REUMANN ENZ - Int. Alvear',
'P06449 - C.S. ALEJANDRO ROST - Santa Teresa'	=>	'P06449 - C.S. ALEJANDRO ROST - Santa Teresa',
'P06446 - C.S. ANCHORENA'	=>	'P06446 - C.S. ANCHORENA',
'P06459 - C.S. PICHI HUINCA'	=>	'P06459 - C.S. PICHI HUINCA',
'P03815 - HOSP. DRA. CECILIA GRIERSON - Telen'	=>	'P03815 - HOSP. DRA. CECILIA GRIERSON - Telen',
'P03814 - C.S. LOS HORNOS - Santa Rosa'	=>	'P03814 - C.S. LOS HORNOS - Santa Rosa',
'P03813 - C.S. DR. GUILLERMO FURST - COMERCIO - San...'	=>	'P03813 - C.S. DR. GUILLERMO FURST - COMERCIO - San...',
'P03812 - C.S. ZONA NORTE - Santa Rosa'	=>	'P03812 - C.S. ZONA NORTE - Santa Rosa',
'P03811 - C.S. MATADEROS - Santa Rosa'	=>	'P03811 - C.S. MATADEROS - Santa Rosa',
'P06452 - C.S. SRITA MECHA'	=>	'P06452 - C.S. SRITA MECHA',
'P03807 - HOSP. ARMANDO PARODI - Parera'	=>	'P03807 - HOSP. ARMANDO PARODI - Parera',
'P06456 - C.S. DORILA'	=>	'P06456 - C.S. DORILA',
'P00008 - Servicio Movil de Salud - I'	=>	'P00008 - Servicio Movil de Salud - I',
'P06447 - HOSP. DR. LUCIO MOLAS - Santa Rosa'	=>	'P06447 - HOSP. DR. LUCIO MOLAS - Santa Rosa',
'P06479 - HOSP. PADRE BUODO - General Acha'	=>	'P06479 - HOSP. PADRE BUODO - General Acha',
'P06477 - C.S. MARIA HOLLSMAN - Col. Santa María'	=>	'P06477 - C.S. MARIA HOLLSMAN - Col. Santa María',
'P06475 - HOSP. DR. JORGE AHUAD - Col. 25 de Mayo'	=>	'P06475 - HOSP. DR. JORGE AHUAD - Col. 25 de Mayo',
'P06473 - C.S. LA REFORMA'	=>	'P06473 - C.S. LA REFORMA',
'P06471 - C.S. ABRAMO'	=>	'P06471 - C.S. ABRAMO',
'P03804 - HOSP. DR. JOSE INGENIEROS - Miguel Cané'	=>	'P03804 - HOSP. DR. JOSE INGENIEROS - Miguel Cané',
'P03034 - HOSP. SEGUNDO TALADRIZ'	=>	'P03034 - HOSP. SEGUNDO TALADRIZ',
'P03793 - HOSP. VILFRID BARON'	=>	'P03793 - HOSP. VILFRID BARON',
'P06483 - DRA. LUISA DE PISTARINI - Victorica'	=>	'P06483 - DRA. LUISA DE PISTARINI - Victorica',
'P03809 - HOSP. DR. ATILIO CALANDRI - Quemú Quemú'	=>	'P03809 - HOSP. DR. ATILIO CALANDRI - Quemú Quemú',
'P01175 - C.S. VILLA PARQUE'	=>	'P01175 - C.S. VILLA PARQUE',
'P06493 - CIC'	=>	'P06493 - CIC',
'P06467 - HOSP. DR. GUILLERMO RAWSON - Trenel'	=>	'P06467 - HOSP. DR. GUILLERMO RAWSON - Trenel',
'P06498 - C.S. RECONVERSION - Santa Rosa'	=>	'P06498 - C.S. RECONVERSION - Santa Rosa',
'P00004 - POSTA ATILIO CRENNA - INTENDENTE ALVEAR'	=>	'P00004 - POSTA ATILIO CRENNA - INTENDENTE ALVEAR',
'P06497 - C.S.  OBREROS DE LA CONSTRUCCION ALEJANDR...'	=>	'P06497 - C.S.  OBREROS DE LA CONSTRUCCION ALEJANDR...',
'P06496 - C.S. Cuchillo Co'	=>	'P06496 - C.S. Cuchillo Co',
'P00002 - C.S. Barrio Norte'	=>	'P00002 - C.S. Barrio Norte',
'P00001 - Movil Odontologico'	=>	'P00001 - Movil Odontologico',
'P06494 - C.S. Salvador Abudara'	=>	'P06494 - C.S. Salvador Abudara',
'P06495 - C.S. Barrio Plan Federal'	=>	'P06495 - C.S. Barrio Plan Federal',
'P03810 - HOSP. DR. DIEGO MORON - Rolon'	=>	'P03810 - HOSP. DR. DIEGO MORON - Rolon',
);